var FormValidation = function () {

    var accountReceivedForm = function () {
        var form1 = $('#accountReceivedForm');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                ecommerce_status: {
                    required: true,
                },
                ecommerce_sub_status: {
                    required: true,
                },
                remarks: {
                    required: true,
                },
            },
            messages: {
                ecommerce_status: {
                    required: "This Field is Required"
                },
                ecommerce_sub_status: {
                    required: "Select Order Sub Status"
                },
                remarks:{
                    required: "Enter Remarks"  
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                $('#loading').hide();
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    var merchandiserAssignVendor = function () {
        var form1 = $('#merchandiserAssignVendor');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                ecommerce_status: {
                    required: true,
                },
                ecommerce_sub_status: {
                    required: true,
                },
                vendor_id: {
                    required: true,
                },
                ponumber: {
                    required: true,
                },
                processing_date: {
                    required: true,
                },
                po_weight:{
                    required: true,  
                },
                remarks:{
                    required: true,
                }
            },
			messages: {
                ecommerce_status: {
                    required: "This Field is Required"
                },
                ecommerce_sub_status: {
                    required: "This Field is Required"
                },
                vendor_id: {
                    required: "Vendor is Required"
                },
                ponumber: {
                    required: "PO No. is Required"
                },
                processing_date: {
                    required: "PO Date is Required"
                },
                po_weight:{
                    required: "PO Weight is Required",  
                },
                remarks:{
                    required: "Please Enter Remarks",    
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                $('#loading').hide();
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    var merchandiserreceived = function () {
        var form1 = $('#merchandiserreceived');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                remarks:{
                    required: true,
                }
            },
            messages: {
                remarks:{
                    required: "Please Enter Remarks",    
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                $('#loading').hide();
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    var merchandiserqc = function () {
        var form1 = $('#merchandiserqc');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: ":hidden",
            rules: {
                check_certificate:{
                    required: true,
                },
                check_image:{
                    required: true,  
                },
                check_size:{
                    required: true,    
                },
                check_weight:{
                    required: true,  
                },
                check_finishing:{
                    required: true,    
                },
                check_locking:{
                    required: true,      
                },
                check_diamaondquality:{
                    required: true,        
                },
                ecommerce_sub_status:{
                    required:true
                },
                remarks:{
                    required:true  
                },
                product_weight:{
                    required:true    
                },
                product_size:{
                    required:true 
                }
            },
            messages: {
                check_certificate:{
                    required: "Check Hallmark Or Certificate",    
                },
                check_image:{
                    required: "Check Product By Images",    
                },
                check_size:{
                    required: "Check Product Size",    
                },
                check_weight:{
                    required: "Check Product Weight",    
                },
                check_finishing:{
                    required: "Check Product Finishing",    
                },
                check_locking:{
                    required: "Check Product Locking",      
                },
                check_diamaondquality:{
                    required: "Check Diamond Quality",      
                },
                ecommerce_sub_status:{
                    required: "Select Product Sub Status",        
                },
                remarks:{
                    required: "Enter Remarks",
                },
                product_weight:{
                    required : "Enter Weight"  
                },
                product_size:{
                    required : "Enter Size"    
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                $('#loading').hide();
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
                
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    var operactiondispatch = function () {
        var form1 = $('#operactiondispatch');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                way_bill_no:{
                    required: true,
                },
                remarks:{
                    required: true,  
                }
            },
            messages: {
                way_bill_no:{
                    required: "Enter AWB No",    
                },
                remarks:{
                    required: "Enter Remarks",
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (event, validator) {
                $('#loading').hide();
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    var operactiondispathcstatus = function () {
        var form1 = $('#operactiondispathcstatus');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                ecommerce_sub_status:{
                    required: true,
                },
                remarks:{
                    required: true,  
                }
            },
            messages: {
                ecommerce_sub_status:{
                    required: "Select Order Sub Status",    
                },
                remarks:{
                    required: "Enter Remarks",
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (event, validator) {
                $('#loading').hide();
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

     var vendorsettlememnt = function () {
        var form1 = $('#vendorsettlememnt');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: ":hidden",
            rules: {
                settlement_type:{
                    required: true,
                },
                invoice_no:{
                    required: true,  
                },
                product_weight:{
                    required: true,
                }
            },
            messages: {
                settlement_type:{
                    required: "Select Settlement Type",    
                },
                invoice_no:{
                    required: "Receipt or Invoice No is required",      
                },
                product_weight:{
                    required: "Product Weight is required",        
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                $('#loading').hide();
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                formsubmit(form);
            }
        });
    }
    
    
	return {
        init: function () {
            accountReceivedForm();
            merchandiserAssignVendor();
            merchandiserreceived();
            merchandiserqc();
            operactiondispatch();
            operactiondispathcstatus();
            vendorsettlememnt();
        }
    };
}();
