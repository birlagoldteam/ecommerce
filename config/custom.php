<?php 
//$config = array();
return [
	'web'=>"https://www.cherishgold.com",
	'weburl' => 'https://www.cherishgold.com/ecommerce',
	'webparent'  =>'http://localhost/crm',
    'ecommerce_status'=>[1=>[1=>'Accounts Action'],2=>[2=>'Merchandiser Action'],3=>[3=>"Merchandiser Product Received"],4=>[4=>"QC"],5=>[5=>'Account Bill Generation'],6=>[6=>"Dispatch"],7=>[7=>"Shipment"]],
    'ecommerce_sub_status'=>[1=>[1=>'Received',2=>'Failed'],2=>[1=>"Order Placed"],3=>[1=>"Recived"],4=>[1=>"Accepted",2=>"Rejected"],5=>[1=>"Yes"],6=>[1=>"Yes"],7=>[1=>"Success",2=>"Failed"]],
    'role_wise_opeation'=>"",
    'vendor_settlement_type'=>[1=>'Invoice Billing Settlement',2=>"Metal + Labour Settlement",3=>"Metal Settlement"],
    'taxrate'=>[1=>3,2=>5],
];
?>