@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
@endsection
@section('breadcrumb-title')
    Accounts Dashboard    
@endsection
@section('breadcrumb')
    Dashboard    
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('order-details') }}">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $totalOrders->ordercount }}" id="totalorders">0</span>
                    </div>
                    <div class="desc"> Total Generated Orders </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{ route('order-details',[1]) }}">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $ordersSuccess->ordercount }}">0</span></div>
                    <div class="desc"> Payment Received </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $orderFailed->ordercount }}"></span></div>
                    <div class="desc"> Payment Failed </div>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green-dark" href="{{ route('merchandiser-dashboard-data',[2]) }}">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $orderPlacedVendor->ordercount }}">0</span></div>
                    <div class="desc"> Product's Order Placed to Vendor </div>
                </div>
            </a>
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green-turquoise" href="{{ route('merchandiser-dashboard-data',[3]) }}">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $orderReceivedVendor->ordercount }}"></span></div>
                    <div class="desc">Product Received Form Vendor </div>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="{{ route('order-details',[5]) }}">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $ordersInvoiceGenerated->ordercount }}">0</span>
                    </div>
                    <div class="desc"> Tax Invoice Generated </div>
                </div>
            </a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 orange" href="{{ route('final-billing-orders') }}">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $ordersInvoicePending->ordercount }}">0</span>
                    </div>
                    <div class="desc"> Tax Invoice Pending </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $orderVendorSettlement->ordercount }}">0</span>
                    </div>
                    <div class="desc"> Vendor Settlement Completed </div>
                </div>
            </a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 orange" href="{{ route('vendor-settlement-list') }}">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $orderVendorSettlementPending->ordercount }}">0</span> </div>
                    <div class="desc"> Vendor Settlement Pending </div>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection    
@section('page_level_js')
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            //$('#loading').show();
        });
        /*$(document).load(function () {
            alert("");
            var totalOrders = {!! $totalOrders !!};
            console.log(totalOrders);
            $('#totalorders').html(totalOrders.orderamount+'/'+totalOrders.ordercount);
        });*/

    </script>
@endsection          
                