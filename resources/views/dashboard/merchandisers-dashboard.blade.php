@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
@endsection
@section('breadcrumb-title')
    Merchandiser Dashboard    
@endsection
@section('breadcrumb')
    Dashboard    
@endsection
@section('content')    
    <div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('order-details',[1]) }}">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">

                            <span data-counter="counterup" data-value="{{ $totalOrder }}"></span>
                        </div>
                      
                        <div class="desc">Total Received Orders </div>
                    </div>
                </a>
            </div>
            
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{ route('order-details',[2]) }}">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $placedOrder->cnt }}">0</span></div>
                        <div class="desc"> Placed Orders </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="{{url('merchandiser-order')}}">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $pendingOrder->cnt }}"></span></div>
                        <div class="desc"> Pending Orders </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 green-dark" href="{{ route('merchandiser-dashboard-data',[3]) }}">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="{{ 
                                $receivedProductFromVendor->cnt }}">0</span>
                            </div>
                            <div class="desc"> Received Products From Vendors </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 green-dark" href="{{ route('merchandiser-dashboard-data',[2,1,'pending']) }}">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="{{ 
                                $pendingProductReceivedFromVendor->cnt }}">0</span>
                            </div>
                            <div class="desc"> Pending Products Received From Vendor </div>
                        </div>
                    </a>
                </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{ route('merchandiser-dashboard-data',[4]) }}">
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="{{ ($productQcDone->cnt) }}">0</span>
                            </div>
                            <div class="desc">Product QC Done </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 orange" href="{{ route('merchandiser-dashboard-data',[3,1,'pending']) }}">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="{{ $productQcPending->cnt }}">0</span>
                            </div>
                            <div class="desc"> Product QC Pending </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="0">0</span></div>
                            <div class="desc">QC Rejected Products </div>
                        </div>
                    </a>
                </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection    
@section('page_level_js')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('#loading').hide();
        });
    </script>
@endsection  