    @extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
@endsection
@section('breadcrumb-title')
    Operaction Dashboard    
@endsection
@section('breadcrumb')
    Dashboard    
@endsection
@section('content')    
    <div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{ route('order-details') }}">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderGenerate->ordercount }}">0</span></div>
                        <div class="desc">Total Order Generated</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="#">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderFailed->ordercount }}"></span></div>
                        <div class="desc">Orders Payment Failed</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-dark" href="{{ route('order-details',[1]) }}">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderReceived->ordercount }}">0</span>
                        </div>
                        <div class="desc">Orders Payment Received</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{ route('order-details',[2]) }}">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderPlaced->cnt }}">0</span></div>
                        <div class="desc">Merchandiser Order Placed</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="{{ route('order-details',[1,'pending']) }}">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderPending->cnt }}"></span></div>
                        <div class="desc">Merchandiser Order Pending</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{ route('order-details',[4]) }}">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderQcDone->cnt }}">0</span></div>
                        <div class="desc">Merchandiser QC Done</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="#">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderQcRejected->cnt }}"></span></div>
                        <div class="desc">Merchandiser QC Rejected</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="{{ route('order-details',[4,'pending']) }}">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderInvoicePending->cnt }}"></span></div>
                        <div class="desc">Pending For Final Invoice</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{ route('operation-dispatch') }}">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderReadyShipment->cnt }}">0</span></div>
                        <div class="desc">Product Ready for Shipment</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="{{ route('order-details',[6]) }}">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderShipped->cnt }}"></span></div>
                        <div class="desc">Order Shipped</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-dark" href="{{ route('order-details',[7]) }}">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderShippedSuccess->cnt }}">0</span>
                        </div>
                        <div class="desc">Order Shipment Success</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-dark" href="#">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $orderShippedFailed->cnt }}">0</span>
                        </div>
                        <div class="desc">Order Shipment Failed</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection    
@section('page_level_js')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('#loading').hide();
        });
    </script>
@endsection  