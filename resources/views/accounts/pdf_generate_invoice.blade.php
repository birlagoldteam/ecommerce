<style>
    body {
        font-family: sans-serif;
    }
    td  { 
        vertical-align: top; 
    }
    .head {
        font-weight:bold;
        font-size: 12px;
    }

    .type {
        font-weight:bold;
        font-style: italic;
        font-size: 10px;
    }

    .totals {
        font-weight:bold;
    }
    .items td {
        border: 0.1mm solid #000000;
    }
    table thead td {
        background-color: #EEEEEE;
        text-align: center;
        font-weight:bold;
    }
    .items td.blanktotal {
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }
    .items td.totals {
        text-align: right;
        border: 0.1mm solid #000000;
    }

    tr.cancelled td {
        text-decoration: line-through;
        font-weight: bold;
        font-style: italic;
    }
</style>
<table class="items" width="100%" style="font-size: 9pt;" cellpadding="5">
    <tbody>
        <tr>
            <td align="center" width="50%">
                <!--<h1>CHERISH GOLD</h1>-->
               <style type="text/css">
                    .upn {
                      background-image:url("{{ asset('public/img/ecommerce_logo.png') }}");
                      background-repeat:no-repeat;
                      width:700px;
                      height:342px;
                      position:absolute;
                   }
                </style>
                <div class="upn"></div>
            </td>
            <td width="50%">
                <strong>Birla Gold And Precious Metals Limited</strong><br />
                Registered address : <br />
                316, Crystal Point, Near D.N.Nagar Metro Station,<br />
                New Link Road, Andheri(W),<br />
                Mumbai-400053
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h4 align="center"><strong>TAX INVOICE FOR SUPPLY OF GOODS</strong></h4>
            </td>
        </tr>
        <tr>
            <td>
                <strong>
                    Billed and Shipped to:<br />
                    {{ ucfirst($data->customer->first_name) }}&nbsp;{{ ucfirst($data->customer->last_name) }}</strong><br />
                    {{ $data->shipping_add }},<br />
                    {{ $data->slandmark }} {{ $data->scity }} {{ $data->sstate }} {{ $data->pincode }}<br /><br />
                    Mobile No: {{ $data->scontact }}<br />
                    Place Of Supply:  &nbsp;&nbsp;&nbsp;{{ $data->sstate }}
                </strong>
            </td>
            <td>
                Invoice No.&nbsp;&nbsp;&nbsp;&nbsp;: BGAPML\SG/{{ request()->invoice_no }}<br />
                Invoice Date &nbsp;&nbsp;: {{ date('F d, Y') }}<br />
                GST No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:27AAECA2444H<br />
                PAN No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:AAECA2444H<br />
                CIN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:U51900MH2001PLC133454<br /><br />
                Whether Reverse Charge applicable: NO
            </td>
        </tr>
    </tbody>
</table>

<table class="items" width="100%" style="font-size: 9pt;" cellpadding="5">
    <thead>
        <tr>
            <td>Sr.No.</td>
            <td>Items</td>
            <td>HSN Code</td>
            <td>Gross Weight(Gms)</td>
            <td>Net Weight(Gms)</td>
            <td>Diamond Carat</td>
            <td>Amount(Rs.)</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td> 22K New Gold Ornaments:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php
            $totalGrossWeight = 0;
            $totalNetWeight = 0;
            $totalAmount = 0;
        ?>

        @foreach($data->shoppingcart as $i=>$value)
        	<tr>
	            <td>{{ ($i+1) }}</td>
	            <td>{{ $value['product']['product_sku'] }} : {{ $value['product']['product_name'] }}</td>
	            @if($i == 0)
                <td align="right" rowspan="{{ count($data->shoppingcart) }}">7113</td>
                @endif
	            <td align="right">
		            <?php
		            	$totalGrossWeight += $value->weight;
		            	echo $value->weight;
		            ?>
	            </td>
	            <td align="right">
	                <?php
	                    $totalNetWeight += $value->weight;
	                    $totalAmount += $value->weight*$value->goldprice;
	                    echo $value->weight;
	                ?>
	            </td>
	            <td></td>
	            <td></td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">{{ $totalGrossWeight }}</td>
            <td align="right">{{ $totalNetWeight }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td align="right">{{ $totalAmount }}</td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                <strong>Taxable Value Rs.</strong>
            </td>
            <td align="right"><strong>{{ $totalAmount }}</strong></td>
        </tr>
        @if($data->sstate == 'Maharashtra')
        <tr>
            <td align="right" colspan="6">
                CGST 1.5%
            </td>
            <td align="right">
	            <?php
	                $cgstRate = $totalAmount*1.5/100;
	            	echo number_format($cgstRate,2);
	            ?>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                SGST 1.5%
            </td>
            <td align="right">
            	<?php
                    $sgstRate = $totalAmount*1.5/100;
					echo number_format($sgstRate,2);
				?>
            </td>
        </tr>
        @else
        <tr>
            <td align="right" colspan="6">
                IGST 1.5%
            </td>
            <td width="10%" align="right">
                <?php
                    $igstRate = $totalAmount*1.5/100;
                    echo number_format($igstRate,2);
                ?>
            </td>
        </tr>
        @endif
        <tr>
            <td align="right" colspan="6">
                <strong>Total Amount Rs.</strong>
            </td>
            <td align="right">
                <?php
                    if($data->sstate == 'Maharashtra') {
                        $totalAmount = $totalAmount + $cgstRate + $sgstRate;
                    } else {
                        $totalAmount = $totalAmount + $igstRate;
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                <strong>Rounded Off Rs.</strong>
            </td>
            <td align="right">
                {{ number_format($totalAmount) }}
            </td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                <strong>Advance Received</strong>
            </td>
            <td align="right">{{ $data->amount_received }}</td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                <strong>Discount Voucher Issued</strong>
            </td>
            <td align="right">
                <?php
                    if(!empty($data->voucher_amount)){
                        $totalAmount = $totalAmount - $data->voucher_amount;
                        //echo $value->voucher_amount;
                    } else {
                        //echo '-';
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                <strong>Balance Receivable</strong>
            </td>
            <td align="right">
                <strong>{{ number_format($totalAmount) }}</strong>
            </td>
        </tr>
    </tbody>
</table>
<table class="items" width="100%" style="font-size: 9pt;" cellpadding="5">
    <tbody>
        <tr>
            <td>
                <br />
                Invoice Amount in Words:<br />
                <?php
                    $amountInWords = getIndianCurrency(str_replace(',', '', number_format($totalAmount)));
                ?>
                <strong>Rupees {{ ucwords($amountInWords) }} </strong>
                <h6>For Terms and conditions, kindly refer to www.cherishgold.com</h6>
            </td>
        </tr>
    </tbody>
</table>
<table class="items" width="100%" style="font-size: 9pt;" cellpadding="5">
    <tbody>
        <tr>
            <td width="70%">
                <h5><strong>Received above mentioned jewellery as described and in good condition</strong></h5><br /><br />
                <h5><strong>Buyer&#39;s Signature</strong></h5>
            </td>
            <td width="30%" align="right">
                <h5>For Birla Gold And Precious Metals Ltd.</h5><br /><br />
                <h5><strong>Authorised Signatory</strong></h5>
            </td>
        </tr>
    </tbody>
</table>