@extends('layouts.app')
@section('page_title')
    Generate Invoice
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <li class="active">Generate Invoice</li>    
@endsection
@section('content')
 <form action="{{ route('invoice-generated') }}" target="_blank" method="post" role="search" id="frm">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="hidden" name="order_id" id="order_id" value="{{ $data->order_id }}">
    <input type="hidden" name="invoice_generated" id="order_id" value="1">

    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Generate Invoice</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="50%" align="center">
                                    <style type="text/css">
                                        .upn {
                                          background-image:url("{{ asset('public/img/ecommerce_logo.png') }}");
                                          background-repeat:no-repeat;
                                          width:700px;
                                          height:342px;
                                          position:absolute;
                                       }
                                    </style>
                                    <div class="upn" align="center"></div>
                                </td>
                                <td width="50%">
                                    <strong>Birla Gold And Precious Metals Limited</strong><br />
                                    Registered address : <br />
                                    316, Crystal Point, Near D.N.Nagar Metro Station,<br />
                                    New Link Road, Andheri(W),<br />
                                    Mumbai-400053
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4 class="text-center"><strong>TAX INVOICE FOR SUPPLY OF GOODS</strong></h4>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%">
                                    <strong>
                                        Billed and Shipped to:<br />
                                        {{ ucfirst($data->customer->first_name) }}&nbsp;{{ ucfirst($data->customer->last_name) }}</strong><br />
                                        {{ $data->shipping_add }},<br />
                                        {{ $data->slandmark }} {{ $data->scity }} {{ $data->sstate }} {{ $data->pincode }}<br /><br />
                                        Mobile No: {{ $data->scontact }}<br />
                                        Place Of Supply:  &nbsp;&nbsp;&nbsp;{{ $data->sstate }}
                                    </strong>
                                </td>
                                <td width="50%">
                                    Invoice No.&nbsp;&nbsp;&nbsp;&nbsp;: BGAPML\SG/<input type="text" name="invoice_no"><br />
                                    Invoice Date &nbsp;&nbsp;: {{ date('F d, Y') }}<br />
                                    GST No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:27AAECA2444H<br />
                                    PAN No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:AAECA2444H<br />
                                    CIN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:U51900MH2001PLC133454<br /><br />
                                    Whether Reverse Charge applicable: NO
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th width="10%">Sr.No.</th>
                                <th width="40%">Items</th>
                                <th width="10%">HSN Code</th>
                                <th width="10%">Gross Weight(Gms)</th>
                                <th width="10%">Net Weight(Gms)</th>
                                <th width="10%">Diamond Carat</th>
                                <th width="10%">Amount(Rs.)</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td> 22K New Gold Ornaments:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                                $totalGrossWeight = 0;
                                $totalNetWeight = 0;
                                $totalAmount = 0;
                            ?>
                            @foreach($data->shoppingcart as $i=>$value)
                            <tr>
                                <!-- Products list -->
                                <td>{{ $i+1 }}</td>
                                <td>{{ $value['product']['product_sku'] }} : {{ $value['product']['product_name'] }}</td>
                                @if($i == 0)
                                <td align="right" rowspan="{{ count($data->shoppingcart) }}">7113</td>
                                @endif
                                <td align="right">
                                    <?php $totalGrossWeight += $value->weight ?>
                                    {{ $value->weight }}
                                </td>
                                <td align="right">
                                    <?php
                                        $totalNetWeight += $value->weight;
                                        $totalAmount += $value->weight*$value->goldprice;
                                    ?>
                                    {{ $value->weight }}
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="right">{{ $totalGrossWeight }}</td>
                                <td align="right">{{ $totalNetWeight }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="6"></td>
                                <td align="right">{{ $totalAmount }}</td>
                            </tr>
                            <tr>
                                <td width="90%" align="right" colspan="6" style="border-bottom: 0px solid #ccc !important;">
                                    <strong>Taxable Value Rs.</strong>
                                </td>
                                <td width="10%" align="right"><strong>{{ $totalAmount }}</strong></td>
                            </tr>
                            @if($data->sstate == 'Maharashtra')
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    CGST 1.5%
                                </td>
                                <td width="10%" align="right">
                                    <?php
                                        $cgstRate = $totalAmount*1.5/100;
                                        echo number_format($cgstRate,2);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    SGST 1.5%
                                </td>
                                <td width="10%" align="right">
                                    <?php
                                        $sgstRate = $totalAmount*1.5/100;
                                        echo number_format($sgstRate,2);
                                    ?>
                                </td>
                            </tr>
                            @else
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    IGST 1.5%
                                </td>
                                <td width="10%" align="right">
                                    <?php
                                        $igstRate = $totalAmount*3/100;
                                        echo number_format($igstRate,2);
                                    ?>
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    <strong>Total Amount Rs.</strong>
                                </td>
                                <td width="10%" align="right">
                                    <?php
                                        if($data->sstate == 'Maharashtra') {
                                            $totalAmount = $totalAmount + $cgstRate + $sgstRate;
                                        } else {
                                            $totalAmount = $totalAmount + $igstRate;
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    <strong>Rounded Off Rs.</strong>
                                </td>
                                <td width="10%" align="right">
                                    <?php echo number_format($totalAmount); ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    <strong>Advance Received</strong>
                                </td>
                                <td width="10%" align="right">{{ $value->amount_received }}</td>
                            </tr>
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    <strong>Discount Voucher Issued</strong>
                                </td>
                                <td width="10%" align="right">
                                    <?php
                                        if(!empty($value->voucher_amount)){
                                            $totalAmount = $totalAmount - $value->voucher_amount;
                                            echo $value->voucher_amount;
                                        } else {
                                            echo '-';
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="90%" align="right" colspan="6">
                                    <strong>Balance Receivable</strong>
                                </td>
                                <td width="10%" align="right">
                                    <strong><?php echo number_format($totalAmount); ?></strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    <br />
                                    Invoice Amount in Words:<br />
                                    <?php
                                        $amountInWords = getIndianCurrency(str_replace(',', '', number_format($totalAmount)));
                                    ?>
                                    <strong>Rupees {{ ucwords($amountInWords) }}</strong>
                                    <h6>For Terms and conditions, kindly refer to www.cherishgold.com</h6>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="70%">
                                    <h5><strong>Received above mentioned jewellery as described and in good condition</strong></h5><br /><br /><br />
                                    <h5><strong>Buyer's Signature</strong></h5>
                                </td>
                                <td width="30%" align="right">
                                    <h5>For Birla Gold And Precious Metals Ltd.</h5><br /><br /><br />
                                    <h5><strong>Authorised Signatory</strong></h5>
                                </td>
                                <!-- <img src="{{URL::asset('public/img/ecommerce_logo.png')}}"> -->
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <button type="submit" class="btn green submit">Generate Invoice</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </form>
@endsection
@section('page_level_js')
   
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('.submit').click(function() {
                var invoice_no = $('input[name="invoice_no"]').val();
                if(invoice_no==""){
                    alert("Please Enter Invoice No");
                    return false;    
                }
                var url = "{!! route('invoice-generated',['order_id'=>request()->segment(2),'invoice_no'=>""]) !!}"+invoice_no;
                window.open(url+invoice_no, '_blank');
                window.location.href = "{{ route('check-invoice') }}";
                return false;
            });
        });
    </script>
@endsection