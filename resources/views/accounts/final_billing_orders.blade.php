@extends('layouts.app')
@section('page_title')
    Final Billing Orders
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <li class="active">Final Billing Orders</li>    
@endsection
@section('content')
    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Final Billing Orders</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Order ID</th>
                                <th>Invoice</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data)>0)
                                @foreach($data as $key=>$value)  
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->order_id }}</td>
                                        <td>{{ $value->invoice }}</td>
                                        <td>{{ date('d-m-Y',strtotime($value->order_date)) }}</td>
                                        <td>
                                            {{--@if($value->invoice_generated != 1)--}}
                                                <a href="{{ route('generate-invoice',$value->order_id) }}" class="btn btn-primary">Generate Invoice</a>
                                            {{--@else--}}
                                                <!-- <a href="public/generated_invoices/invoice_{{ $value->order_id }}.pdf" target="_blank" class="btn btn-warning">See Invoice</a> -->
                                            {{--@endif--}}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection