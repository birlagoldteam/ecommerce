@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    
@endsection
@section('breadcrumb')
    <li class="active">Order List</li>    
@endsection
@section('content')
    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Order List</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Order ID</th>
                                <th>Invoice</th>
                                <th>Date</th>
                                <th>Amount </th>
                                <th>Amt Received</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data->count()>0)
                                @foreach($data as $key=>$value)  
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->order_id }}</td>
                                        <td>{{ $value->invoice }}</td>
                                        <td>{{ $value->order_date }}</td>
                                        <td>{{ $value->order_amount }}</td>
                                        <td>{{ $value->amount_received }}</td>
                                        <td><a class="openmodal" data-id="{{ $value->order_id }}" data-invoice="{{ $value->invoice }}">Action</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('save-account') }}" method="post" role="search" id="accountReceivedForm" name="accountReceivedForm">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="order_id" id="order_id" value="">
                    <!-- <input type="hidden" name="account_is_received" id="account_is_received" value="1"> -->
                    <div class="modal-header">
                        <h4 class="modal-title"><span id="showlead">Account Action</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 control-label">Order No:</label>
                                    <label class="col-md-2 control-label" id="disporderno"></label>
                                    <label class="col-md-3 control-label">Invoice No:</label>
                                    <label class="col-md-5 control-label" id="dispinvoiceno"></label>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Order Status:</label>
                                    <div class="col-md-6">  
                                        <select class="form-control" name="ecommerce_status" id="ecommerce_status">
                                            @foreach($status as $key=>$val)
                                                <option value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row showhide">
                                    <label class="col-md-4 control-label">Order Sub Status:</label>
                                    <div class="col-md-6">  
                                        <select class="form-control" name="ecommerce_sub_status" id="ecommerce_sub_status" >
                                            <option value=""> --- Select Sub Status ---</option>
                                            @foreach($substatus as $key=>$val)
                                                <option value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Remarks:</label>
                                    <div class="col-md-6">  
                                        <textarea type="textarea" rows="4" name="remarks" id="remarks" class="form-control" value="" placeholder="Please Enter Verification Interaction " style="resize:vertical;" ></textarea>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green">Save</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>  
    </div>
@endsection    
@section('page_level_js')
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            FormValidation.init();
        });
        function formsubmit(form){
            form.submit();
        }
        $('.openmodal').click(function(){
            $("#accountReceivedForm").validate().resetForm();
            $('.has-error').removeClass('has-error');
            $('#loading').show();
            var id = $(this).data('id');
            var invoice = $(this).data('invoice');
            $('#order_id').val(id);
            $('#disporderno').html(id);
            $('#dispinvoiceno').html(invoice);
            $('#modal').modal('show');
            $('#loading').hide();
        });

        $('#accountReceivedForm').submit(function(){
            $('#loading').show();
        });
    </script>
@endsection