@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <li class="active">Dispatch Order List</li>    
@endsection
@section('content')
    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Dispatch Order List
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Order ID</th>
                                <th>Invoice</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data->count()>0)
                                @foreach($data as $key=>$value)  
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->order_id }}</td>
                                        <td>{{ $value->invoice }}</td>
                                        <td>{{ date('d-m-Y',strtotime($value->order_date)) }}</td>
                                        <td><a class="openmodal" data-id="{{ $value->order_id }}" data-invoice="{{ $value->invoice }}">Action</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('save-operation-dispatch') }}" method="post" role="search" id="operactiondispatch">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="order_id" id="order_id" value="">
                    <div class="modal-header">
                        <h4 class="modal-title"><span id="showlead">Merchandiser Action</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12"> 
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody>
                                            <tr>
                                                <td> Order No </td>
                                                <td id="disporderno"></td>
                                            </tr>
                                            <tr>
                                                <td> Invoice No</td>
                                                <td id="dispinvoiceno"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Call Status</label>
                                <div class="col-md-6">  
                                    <select class="form-control" name="ecommerce_status" id="ecommerce_status">
                                        @foreach($status as $key=>$val)
                                            <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Call Sub Status:</label>
                                <div class="col-md-6">  
                                    <select class="form-control" name="ecommerce_sub_status" id="ecommerce_sub_status" >
                                        @foreach($substatus as $key=>$val)
                                            <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">AWB Bill No:</label>
                                <div class="col-md-6">  
                                    <input type="text" class="form-control" name="way_bill_no">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Remarks:</label>
                                <div class="col-md-6">  
                                    <textarea type="textarea" rows="4" name="remarks" id="remarks" class="form-control" value="" placeholder="Please Enter Remarks" style="resize:vertical;" ></textarea>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green">Save</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>  
    </div>
@endsection    
@section('page_level_js')
    <script src="{{ asset('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            FormValidation.init();
        });
        $('.openmodal').click(function(){
            $('#operactiondispatch :input').not('#ecommerce_status,#ecommerce_sub_status,#token,.meet').val('');
            var id = $(this).data('id');
            var invoice = $(this).data('invoice');
            var cart = $(this).data('cart_id');
            $('#order_id').val(id);
            $('#disporderno').html(id);
            $('#dispinvoiceno').html(invoice);
            $.ajax({
                url: "{{ route('porduct-details') }}",
                data:{'cart_id':cart},  
                type: 'get',
                async:false,
                cache: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success:function(response){
                    
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
            $('#modal').modal('show');

        })
    </script>
@endsection
