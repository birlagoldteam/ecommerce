@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <li class="active">QC Order List</li>    
@endsection
@section('content')
    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>QC Order List</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Order ID</th>
                                <th>Invoice</th>
                                <th>Date</th>
                                <th>Product</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data->count()>0)
                                @foreach($data as $key=>$value)  
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->order_id }}</td>
                                        <td>{{ $value->invoice }}</td>
                                        <td>{{ date('d-m-Y',strtotime($value->order_date)) }}</td>
                                        <td>{{ $value->product_sku }}</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" onclick="$('.showhide').hide();$('.order_'+{{$value->order_id}}).toggle();">Detail</button>
                                        </td>
                                        <!-- <td><a class="openmodal" data-id="{{ $value->order_id }}" data-invoice="{{ $value->invoice }}" data-cart_id="{{ $value->cart_id }}" data-product="{{ $value->product_sku }}">Action</td> -->
                                    </tr>
                                    @foreach($value->shoppingcart as $key=>$shoppingCart)
                                    @if($key == 0)
                                        <tr class="order_{{$value->order_id}} showhide" style="display:none;">
                                            <th>Product</th>
                                            <th>Product Code</th>
                                            <th>Action</th>
                                        </tr>
                                    @endif
                                    <tr class="order_{{$value->order_id}} showhide" style="display:none;">    
                                        <td>{{ $shoppingCart->product->product_name }}</td>
                                        <td>{{ $shoppingCart->product->product_sku }}</td>
                                        <td><a class="openmodal" data-id="{{ $value->order_id }}" data-invoice="{{ $value->invoice }}" data-cart_id="{{ $shoppingCart->cart_id }}" data-product="{{ $shoppingCart->product->product_sku }}">Action</td>

                                    </tr>
                                    @endforeach
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('save-qc-merchandiser') }}" method="post" role="search" id="merchandiserqc">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="order_id" id="order_id" value="">
                    <input type="hidden" name="shoppingcart_id" id="cart_id" value="">
                    <div class="modal-header">
                        <h4 class="modal-title"><span id="showlead">Merchandiser Action</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8"> 
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <td> Order No </td>
                                                    <td id="disporderno"></td>
                                                </tr>
                                                <tr>
                                                    <td> Invoice No</td>
                                                    <td id="dispinvoiceno"></td>
                                                </tr>
                                                <tr>
                                                    <td>Product</td>
                                                    <td id="disproduct"></td>
                                                </tr>
                                                    <td>Product Type</td>
                                                    <td id="dispproducttype"></td>
                                                <tr>
                                                    <td>Category</td>
                                                    <td id="dispcategory"></td>
                                                </tr>
                                                <tr>
                                                    <td>PO & Date</td>
                                                    <td id="disppono"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-12">
                                            <img src="" alt="Smiley face" id="websiteimg" style="max-width: 100%;max-height: 133px;">
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <a href="" target="_blank" id="webistelink">View Details</a> 
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">QC</div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Product Status</label>
                                            <div class="col-md-6">  
                                                <select class="form-control" name="ecommerce_status" id="ecommerce_status">
                                                    @foreach($status as $key=>$val)
                                                        <option value="{{ $key }}">{{ $val }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label product_type"></label>
                                            <div class="col-md-6">  
                                               <div class="radio-list" data-error-container="#form_2_membership_error1">
                                                    <label class="radio-inline">
                                                        <div class="radio"><span><input type="radio" name="check_certificate" value="yes" class="meet"></span></div>Yes
                                                    </label>
                                                    <label class="radio-inline showradio">
                                                        <div class="radio"><span><input type="radio" name="check_certificate" value="no" class="meet"></span></div>No
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error1"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Product By Images</label>
                                            <div class="col-md-6">  
                                                <div class="radio-list" data-error-container="#form_2_membership_error2">
                                                    <label class="radio-inline">
                                                        <div class="radio"><span><input type="radio" name="check_image" value="yes" class="meet"></span></div>Yes
                                                    </label>
                                                    <label class="radio-inline showradio">
                                                        <div class="radio"><span><input type="radio" name="check_image" value="no" class="meet"></span></div>No
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error2"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group checksize" style="display:none;">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Product By Size (<span id="dispsize"></span>)</label>
                                            <div class="col-md-4">  
                                                <div class="radio-list" data-error-container="#form_2_membership_error3">
                                                    <label class="radio-inline">
                                                        <div class="radio"><span><input type="radio" name="check_size" value="yes" class="meet"></span></div>Yes
                                                    </label>
                                                    <label class="radio-inline showradio">
                                                        <div class="radio"><span><input type="radio" name="check_size" value="no" class="meet"></span></div>No
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error3"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <input type="text" name="product_size" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Weight (<span id="dispweight"></span>)</label>
                                            <div class="col-md-4">  
                                               <div class="radio-list" data-error-container="#form_2_membership_error4">
                                                    <label class="radio-inline">
                                                        <div class="radio"><span><input type="radio" name="check_weight" value="yes" class="meet"></span></div>Yes
                                                    </label>
                                                    <label class="radio-inline showradio">
                                                        <div class="radio"><span><input type="radio" name="check_weight" value="no" class="meet"></span></div>No
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error4"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <input type="text" name="product_weight" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Finishing</label>
                                            <div class="col-md-6">  
                                               <div class="radio-list" data-error-container="#form_2_membership_error5">
                                                    <label class="radio-inline">
                                                        <div class="radio"><span><input type="radio" name="check_finishing" value="yes" class="meet"></span></div>Yes
                                                    </label>
                                                    <label class="radio-inline showradio">
                                                        <div class="radio"><span><input type="radio" name="check_finishing" value="no" class="meet"></span></div>No
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error5"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Locking Method</label>
                                            <div class="col-md-6">  
                                               <div class="radio-list" data-error-container="#form_2_membership_error6">
                                                    <label class="radio-inline">
                                                        <div class="radio"><span><input type="radio" name="check_locking" value="yes" class="meet"></span></div>Yes
                                                    </label>
                                                    <label class="radio-inline showradio">
                                                        <div class="radio"><span><input type="radio" name="check_locking" value="no" class="meet"></span></div>No
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error6"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group diamaondquality">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Diamond Quality</label>
                                            <div class="col-md-6">  
                                               <div class="radio-list" data-error-container="#form_2_membership_error7">
                                                    <label class="radio-inline">
                                                        <div class="radio"><span><input type="radio" name="check_diamaondquality" value="yes" class="meet"></span></div>Yes
                                                    </label>
                                                    <label class="radio-inline showradio">
                                                        <div class="radio"><span><input type="radio" name="check_diamaondquality" value="no" class="meet"></span></div>No
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error7"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Product Sub Status</label>
                                            <div class="col-md-6">  
                                                <select class="form-control" name="ecommerce_sub_status" id="ecommerce_sub_status" >
                                                    <option value="">Select</option>
                                                    @foreach($substatus as $key=>$val)
                                                        <option value="{{ $key }}">{{ $val }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Remarks:</label>
                                            <div class="col-md-6">  
                                                <textarea type="textarea" rows="4" name="remarks" id="remarks" class="form-control" value="" placeholder="Please Enter Remarks" style="resize:vertical;" ></textarea>   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-offset-8">
                                                <button type="submit" class="btn green">Save</button>
                                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
@endsection    
@section('page_level_js')
    <script src="{{ asset('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            FormValidation.init();
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
            });
            var order_id = "{{ request()->segment(2) }}";
            if(order_id){
                $('.showhide').hide();
                $('.order_'+order_id).toggle();
                $('.openmodal').attr('data-cart_id',"{{ request()->segment(3) }}").trigger('click');
            }   
            //FormValidation.init();
            //$('#loading').hide();
            //alert("hii");
        });
        var url ="{{ config('custom.web') }}";
        $('.openmodal').click(function(){
            $("#merchandiserqc").validate().resetForm();
            $('.has-error').removeClass('has-error');
            $('#merchandiserqc :input').not('#ecommerce_status,#token,.meet').val('');
            $('input[type=radio]').prop('checked',false)
            var id = $(this).data('id');
            var invoice = $(this).data('invoice');
            var cart = $(this).data('cart_id');
            $('#order_id').val(id);
            $('#cart_id').val(cart);
            $('#disporderno').html(id);
            $('#dispinvoiceno').html(invoice);
            $('#disproduct').html($(this).data('product'));
            $('.checksize,.diamaondquality').hide();
            $('#websiteimg').attr('src',"");
            $('#webistelink').attr('href',"");
            $('#dispweight,#dispsize').html("");


            $.ajax({
                url: "{{ route('porduct-details') }}",
                data:{'cart_id':cart},  
                type: 'get',
                async:false,
                cache: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success:function(response){
                    var type = response['product_type'] == 1 ? "Gold" : "Diamond";
                    var product_type = response['product_type'] == 1 ? "Hallmark" : "Certificate";
                    if(response['product_type'] == 2){
                        $('.diamaondquality').show();
                    }
                    $('.product_type').html(product_type);
                    $('#dispproducttype').html(type);
                    $('#dispcategory').html(response['category']);
                    if((response['size_id']===null)===false){
                        $('.checksize').show();
                    }        
                    $('#websiteimg').attr('src',url+"/img/product/small/"+response['imagename']);

                    $('#webistelink').attr('href',url+"/search?metal=all&category=all&subcategory=all&keyword="+response['product_sku']);
                    $('#dispweight').html(response['weight']);
                    $('#dispsize').html(response['size']);
                    $('#disppono').html(response['po_number']+' ('+response['podate']+')')
                    $('#modal').modal('show');
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
            
        });
        
    
    </script>
@endsection
