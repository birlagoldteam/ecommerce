@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <li class="active">Merchandiser Vendor List</li>    
@endsection
@section('content')
    <div class="row">
       <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Merchandiser Vendor List</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Order ID</th>
                                <th>Invoice</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data)>0)

                                @foreach($data as $key=>$value)  

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->order_id }}</td>
                                        <td>{{ $value->invoice }}</td>
                                        <td>{{ date('d-m-Y',strtotime($value->order_date)) }}</td>
                                        <td>{{ $value->product_sku }}</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" onclick="$('.showhide').hide();$('.order_'+{{$value->order_id}}).toggle();">Detail</button>
                                        </td>                                        
                                    </tr>
                                    @foreach($value->shoppingcart as $key=>$shoppingCart)
                                        @if($key == 0)
                                            <tr class="order_{{$value->order_id}} showhide" style="display:none;">
                                                <th>Product</th>
                                                <th>Product Code</th>
                                                <th>Action</th>
                                            </tr>
                                        @endif
                                        <tr class="order_{{$value->order_id}} showhide" style="display:none;">    
                                            <td>{{ $shoppingCart->product->product_name }}</td>
                                            <td>{{ $shoppingCart->product->product_sku }}</td>

                                            <?php if ($shoppingCart->ecommerce_status =='1') {?>
                                                <td><a class="openmodal" data-id="{{ $value->order_id }}" data-invoice="{{ $value->invoice }}" data-cart_id="{{ $shoppingCart->cart_id }}" data-product="{{ $shoppingCart->product->product_sku }}">Action</td>
                                            <?php } else {?>
                                                <td> Placed </td>
                                            <?php } ?>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('save-merchandiser') }}" method="post" role="search" id="merchandiserAssignVendor" name="merchandiserAssignVendor">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="order_id" id="order_id" value="">
                    <input type="hidden" name="shoppingcart_id" id="cart_id" value="">
                    <div class="modal-header">
                        <h4 class="modal-title"><span id="showlead">Merchandiser Action</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 control-label">Order No : <span id="disporderno"></span></label>
                                    <label class="col-md-5 control-label">Inv No : <span id="dispinvoiceno"></span></label>
                                    <label class="col-md-4 control-label">Product : <span id="disproduct"></span></label>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Product Status:</label>
                                    <div class="col-md-6">  
                                        <select class="form-control" name="ecommerce_status" id="ecommerce_status">
                                            @foreach($status as $key=>$val)
                                                <option value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Product Sub Status:</label>
                                    <div class="col-md-6">  
                                        <select class="form-control" name="ecommerce_sub_status" id="ecommerce_sub_status" >
                                            @foreach($substatus as $key=>$val)
                                                <option value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Vendor:</label>
                                    <div class="col-md-6">  
                                        <select class="form-control" name="vendor_id" id="vendor_id" >
                                             <option value=""> --- Select Sub Status ---</option>
                                            @foreach($vendor as $key=>$val)
                                                <option value="{{ $val->vendor_id }}">{{ $val->vendor_code.' ('.$val->Company_name.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Weight:</label>
                                    <div class="col-md-6">  
                                      
                                        <input type="text" class="form-control" value="" name="po_weight" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">PO Number:</label>
                                    <div class="col-md-6">  
                                      
                                        <input type="text" class="form-control" value="" name="ponumber" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Processing Date:</label>
                                    <div class="col-md-6">  
                                        <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                            <input type="text" class="form-control datepicker" readonly name="processing_date">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-4 control-label">Remarks:</label>
                                    <div class="col-md-6">  
                                        <textarea type="textarea" rows="4" name="remarks" id="remarks" class="form-control" value="" placeholder="Please Enter Remarks" style="resize:vertical;" ></textarea>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green">Save</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>  
    </div>
@endsection    
@section('page_level_js')
    <script src="{{ asset('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            FormValidation.init();
            var start = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                startDate: start,
            });
            //FormValidation.init();
            //$('#loading').hide();
            //alert("hii");
        });
        $('.openmodal').click(function(){
            $('#loading').show();
            $("#merchandiserAssignVendor").validate().resetForm();
            $('.has-error').removeClass('has-error');
            $('#merchandiserAssignVendor :input').not('#ecommerce_status,#ecommerce_sub_status,#token').val('');
            var id = $(this).data('id');
            var invoice = $(this).data('invoice');
            var cart = $(this).data('cart_id');
            $('#order_id').val(id);
            $('#cart_id').val(cart);
            $('#disporderno').html(id);
            $('#dispinvoiceno').html(invoice);
            $('#disproduct').html($(this).data('product'));
            $('#modal').modal('show');
            $('#loading').hide();
        });

        $('#merchandiserAssignVendor').submit(function(){
            $('#loading').show();
        });
        function formsubmit(form){
            form.submit();
        }
    </script>
@endsection