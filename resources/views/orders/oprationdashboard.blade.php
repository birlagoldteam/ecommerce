@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <li class="active">Merchandiser Vendor List</li>    
@endsection
@section('content')
    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Merchandiser Dashboard</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('order-details',['pay_received',1]) }}">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">

                            <span data-counter="counterup" data-value="{{ $totalOrder }}"></span>
                        </div>
                      
                        <div class="desc">Total Received Orders </div>
                    </div>
                </a>
            </div>
            
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{url('merchantppostorder')}}">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $placedOrder->cnt }}">0</span></div>
                        <div class="desc"> Placed Orders </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="{{url('merchandiser-order')}}">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $pendingOrder->cnt }}"></span></div>
                        <div class="desc"> Pending Orders </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-dark" href="#">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ 
                            $receivedProductFromVendor->cnt }}">0</span>
                        </div>
                        <div class="desc"> Received Products From Vendors </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-dark" href="{{ route('merchandiser-received') }}">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ 
                            $pendingProductReceivedFromVendor->cnt }}">0</span>
                        </div>
                        <div class="desc"> Pending Products Received From Vendor </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="{{url('qcdoneorderlistde')}}">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ ($productQcDone->cnt) }}">0</span>
                        </div>
                        <div class="desc">Product QC Done </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 orange" href="{{ route('merchandiser-qc') }}">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $productQcPending->cnt }}">0</span>
                        </div>
                        <div class="desc"> Product QC Pending </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="{{url('rejectprolists')}}">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ count($rejectpro) }}">0</span></div>
                        <div class="desc">QC Rejected Products </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection    
@section('page_level_js')
    <script type="text/javascript">
        jQuery(document).ready(function () {
         
        });
    </script>
@endsection