<style>
    body {
        font-family: sans-serif;
    }
    td  { 
        vertical-align: top; 
    }
    .head {
        font-weight:bold;
        font-size: 12px;
    }

    .type {
        font-weight:bold;
        font-style: italic;
        font-size: 10px;
    }

    .totals {
        font-weight:bold;
    }
    .items td {
        border: 0.1mm solid #000000;
    }
    table thead td {
        background-color: #EEEEEE;
        text-align: center;
        font-weight:bold;
    }
    .items td.blanktotal {
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }
    .items td.totals {
        text-align: right;
        border: 0.1mm solid #000000;
    }

    tr.cancelled td {
        text-decoration: line-through;
        font-weight: bold;
        font-style: italic;
    }
</style>
<!-- <table class="items" width="100%" style="font-size: 9pt;" cellpadding="5">
    <tbody>
        <tr>
            <td align="center">
               <style type="text/css">
                    .upn {
                      background-image:url("{{ asset('public/img/ecommerce_logo.png') }}");
                      background-repeat:no-repeat;
                      width:700px;
                      height:342px;
                      position:absolute;
                   }
                </style>
                <div class="upn"></div>
            </td>
        </tr>
    </tbody>
</table> -->
<table width="100%" border="1">
    <tr>
        <td width="100%">
            <table width="100%">
                <tr>
                    <td width="50%" align="center">
                        <style type="text/css">
                            .upn {
                              background-image:url("{{ asset('public/img/ecommerce_logo.png') }}");
                              background-repeat:no-repeat;
                              width:700px;
                              height:342px;
                              position:absolute;
                           }
                        </style>
                        <div class="upn" align="center"></div>
                    </td>
                    <td width="50%">
                        <strong>Birla Gold And Precious Metals Limited</strong><br />
                        Registered address : <br />
                        316, Crystal Point, Near D.N.Nagar Metro Station,<br />
                        New Link Road, Andheri(W),<br />
                        Mumbai-400053
                    </td>
                </tr>
            </table>
            <table width="100%" border="1">
                 <tr>
                    <td>
                        <h4 style="text-align: center;"><strong>Purchase Order</strong></h4>
                    </td>
                </tr>
            </table>
            <table width="100%" border="1">
                 <tr>
                    <td width="50%">
                        TO:<br>
                        &nbsp;{{ $data->vendor->Company_name }}<br>
                        &nbsp;{{ $data->vendor->work_address }}<br>
                        &nbsp;{{ $data->vendor->work_address1 }}<br>
                        &nbsp;{{ $data->vendor->work_city }} {{ $data->vendor->work_pincode }}<br>
                        &nbsp;{{ $data->vendor->work_pincode }}<br>
                    </td>
                    <td width="50%">
                        &nbsp;PO Number : {{ $data->po_number }}<br>
                        &nbsp;Dated : {{ date('d-m-Y') }}
                    </td>
                </tr>
            </table><br>
            <table border=1 width="100%">
                <thead>
                    <tr>
                        <th>Sr No.</th>
                        <th>Description</th>
                        <th>Code</th>
                        <th>QTY</th>
                        <th>Grs. Wt gms</th>
                        <th>Net Wt. gms</th>
                        <th>Diamond Details</th>
                        <th>Diamond Wt.</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $diamondweight="";
                    if(!empty($data->diamond_details)){
                        $stoneArray = json_decode($data->diamond_details,1);
                        if(is_array($stoneArray)){
                            if(isset($stoneArray[0]['Productdiamond']['stone_weight'])){
                                $diamondweight = $stoneArray[0]['Productdiamond']['stone_weight'];
                            }
                        }
                    }
                    $sicj = "";
                    $sicj = (!empty($data->clarity)) ? $data->clarity : "";
                    if(!empty($sicj)){
                        $sicj.='_'.$data->color;
                    }?>
                    
                    <tr>
                        <td>1</td>
                        <td>{{ $data->product->product_name }}</td>
                        <td>{{ $data->product->product_sku }}</td>
                        <td>{{ $data->quantity }}</td>
                        <td>{{ $data->po_weight }}</td>
                        <td>{{ $data->po_weight }}</td>
                        <td>{{ $sicj }}</td>
                        <td>{{ $diamondweight }}</td>
                    </tr>
                </tbody>
            </table>
            <br><br><br><br><br><br><br><br><br><br>
            <table width="100%">
                <tr>
                    <td>
                        <u>Terms & Conditions:</u>
                        <table>
                            <tr>
                                <td>1</td>
                                <td>Materials mentioned above to be in good order & conditions.</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Damaged goods will not be accepted.</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Warranty of all the above products will be as per the manufacturer</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>It becomes the liability of the supplier to replace or repair the goods ,if any queries regarding the product arises within the warranty period</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Kindly make invoicing as per the billing Company name mentioned on the purchase order copy</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Payments are released/to be collected from our office by hand</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Any Type of charges of extra taxes leived, which are not mentioned in the purchase order will not be entertained</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>No changes in the payment terms,which are mentioned in the purchase order & agreement</td>
                            </tr>            
                        </table>
                    </td>
                </tr>
            </table>
            <br><br><br>
            <table>
                <tr>
                    <td  style="text-align: left;">This is computer Generated PO So no Signature required</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

