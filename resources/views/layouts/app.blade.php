<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Cherish Gold | Ecommerce @yield('page_title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Super Admin Dashboard for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <link href="{{ asset('public/fonts/fonts.css') }}" rel="stylesheet" type="text/css" />
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('public/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/layouts/layout5/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/layouts/layout5/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/custom/style.css') }}" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="{{ asset('public/favicon.ico') }}" /> 
         <script type="text/javascript">
            window.onload = function() {
                $("#loading-image").fadeOut("500",function () {
                    $('#loading').css("display", "none");
                });
            }
        </script>
        @yield('page_level_style_top')
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <div class="loading" id="loading"> 
            <img class="loading-image" id="loading-image" src="{{ asset('public/img/ajax-loading.gif') }}" alt="Loading..." />
        </div>
        <div class="wrapper">
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        @include('includes.header')
                        <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                            @include('includes.sidebar')
                        </div>
                    </div>
                </nav>
            </header>
            <div class="container-fluid">
                <div class="page-content">
                    <div id="content-body">
                        <div class="breadcrumbs">
                            <div class="col-md-3">
                                <h1>@yield('breadcrumb-title')</h1>
                            </div>
                            <div class="col-md-6">
                                @if(Session::has('alert'))
                                    <div class="alert text-center alert-{{ Session::get('alert') }} message">
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <ol class="breadcrumb">

                                    <li>
                                        <a href="{{ route('dashboard-url') }}">Home</a>
                                    </li>
                                    @yield('breadcrumb')
                                </ol>
                            </div>
                        </div>
                        <div class="content-data">
                            @yield('content')
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @include('includes.footer')
            </div>
        </div>
        <script src="{{ asset('public/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
       <script src="{{ asset('public/assets/layouts/layout5/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/validation/jquery.validate.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/validation/form-validation.js') }}" type="text/javascript"></script>
        @yield('page_level_js')
    </body>
</html>