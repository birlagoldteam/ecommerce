@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
    <li class="active">Order Product Details</li>    
@endsection
@section('content')
    <?php
        $callstatus = config('custom.ecommerce_status');
        $callsubstatus = config('custom.ecommerce_sub_status');
        $product=[];
    ?>
    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Order List</div>
            </div>
            <div class="portlet-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-4">
                                <strong>Customer Name - </strong>{{ ucwords($data['order']['customer']['first_name'].' '.$data['order']['customer']['last_name']) }}
                            </div>
                            <div class="col-md-4">
                                <strong>Order Invoice - </strong>{{ $data['order']['invoice'] }}
                            </div>
                            <div class="col-md-4">
                                <strong>Mobile No - </strong>{{ $data['order']['customer']['phone_no'] }}
                            </div>
                        </div>
                        <div class="row">
                            @if($data['order']->ecommerce_status >=5)
                                <div class="col-md-4">
                                    <strong>Sale Invoice - </strong><a href="{{ asset('') }}/public/generated_invoices/invoice_{{ $data['order']->order_id }}.pdf" target="_blank" class="btn btn-warning">Invoice</a>
                                </div>
                            @endif
                            @if(!empty($data['order']->way_bill_no))
                                <div class="col-md-4">
                                    <strong>AWB Bill - </strong>{{ $data['order']->way_bill_no }}
                                </div>
                            @endif
                            
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Product Details</h4>
                                <div class="table-responsive"> 
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Product</td>
                                            <td>Order Weight</td>
                                            <td>Product Status</td>
                                            <td>Product Sub Status</td>
                                            <td>PO No / PO Date / PO Weight</td>
                                            <td>PO File</td>
                                            <td>QC</td>
                                        </tr>
                                    </thead>
                                    <tbody id="productdetails">
                                        @foreach($data['product'] as $key=>$value)
                                        <tr>
                                            <td>{{ $key+1 }}</td> 
                                            <td><a href="{{config('custom.web')}}/search?metal=all&category=all&subcategory=all&keyword={{ ($value['product_sku'])}}" target="_blank">{{ $value['product_sku'] }}</a></td> 
                                            <td>{{ $value['weight'] }}</td> 
                                            <td>{{ isset($callstatus[$value['ecommerce_status']]) ? $callstatus[$value['ecommerce_status']][$value['ecommerce_status']] :'-' }}</td> 
                                            <td>{{ isset($callsubstatus[$value['ecommerce_sub_status']]) ? $callsubstatus[$value['ecommerce_sub_status']][$value['ecommerce_sub_status']] : '-' }}</td> 
                                            <td>
                                                @if($value->ecommerce_status>=2)
                                                    {{ $value['po_number'].' / '.$value['po_date'].' / '.$value['po_weight'] }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if($value->ecommerce_status>=2)
                                                    &nbsp;
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if($value->ecommerce_status>=4)
                                                    <p type="text" data-toggle="modal" onclick="viewremarks({{ $key }})" data-target="#remarks_interaction" class="btn purple"><i class="fa fa-envelope"></i></p>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        <?php $product[$value['cart_id']] = $value['product_sku'];?>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Interaction Details</h4> 
                                <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Created Date</td>
                                            <td>Created By</td>
                                            <td>Role</td>
                                            <td>Product Status</td>
                                            <td>Sub Status</td>
                                            <td>Remarks</td>
                                        </tr>
                                    </thead>
                                    <tbody id="interaction">
                                        @if(count($data['interaction'])>0)
                                            @foreach($data['interaction'] as $key=>$interactiondata)
                                                <tr><td colspan=6>{{ $product[$key] }}</td></tr>
                                                @foreach($interactiondata as $key=>$value)
                                                    <tr>
                                                        <td width=5%>{{ $key+1 }}</td>
                                                        <td width=10%>{{ $value['int_date'] }}</td>
                                                        <td width=15%>{{ $value['name'] }}</td>
                                                        <td width=15%>{{ $value['role'] }}</td>
                                                        <td width=18%>{{ $callstatus[$value['ecommerce_status']][$value['ecommerce_status']] }}</td>
                                                        <td width=10%>{{ $callsubstatus[$value['ecommerce_status']][$value['ecommerce_sub_status']] }}</td>

                                                         <td data-toggle="popover" data-content="{{ $value['remarks'] }}">{{ substr($value['remarks'], 0, 40) }}...</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @else
                                            <td colspan="7" class="text-center">Not any Interactions yet</td>
                                        @endif
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="remarks_interaction" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-purple"><b>QC of <span id="product_sku"></b></h4>
                </div>
                <div class="modal-body" >
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">QC</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-5 control-label product_type">Certificate / Hallmark</label>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6" id="check_certificate"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-5 control-label">Product By Images</label>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6" id="check_image"></div>
                                </div>
                            </div>
                            <div class="form-group checksize">
                                <div class="row">
                                    <label class="col-md-5 control-label">Product By Size (<span id="product_size"></span>)</label>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-3" id="check_size"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-5 control-label">Weight (<span id="product_weight"></span>)</label>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-3" id="check_weight"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-5 control-label">Finishing</label>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6" id="check_finishing"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-5 control-label">Locking Method</label>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6" id="check_locking"></div>
                                </div>
                            </div>
                            <div class="form-group diamaondquality">
                                <div class="row">
                                    <label class="col-md-5 control-label">Diamond Quality</label>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-6" id="check_diamaondquality"></div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection    
@section('page_level_js')
    <script type="text/javascript">
        var qcdata = {!! $data['product'] !!};
        jQuery(document).ready(function () {
            $('[data-toggle="popover"]').popover({
                placement : 'left',
                trigger : 'hover'
            });
        });

        function viewremarks(arrindex){
            var remarrks = qcdata[arrindex];
            $('#product_sku').html(remarrks['product_sku']);
            var arr = ["check_certificate","check_image","check_size","check_weight","check_finishing","check_diamaondquality","check_locking","product_weight","product_size"];
            $(arr).each(function(index,value){
                console.log(remarrks[value],value);
                if(remarrks[value]!==null){
                    $('#'+value).html(remarrks[value].charAt(0).toUpperCase() + remarrks[value].slice(1));
                }                
            });
            console.log(remarrks['check_size']===null);
            remarrks['check_size']===null ? $('.checksize').hide() : $('.checksize').show();
            remarrks['check_diamaondquality']===null ? $('.diamaondquality').hide() : $('.diamaondquality').show();

        }
    </script>
@endsection