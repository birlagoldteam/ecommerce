@extends('layouts.app')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    <link href="{{ asset('public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}} " rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb-title')
    Order Details
@endsection
@section('breadcrumb')
    <li class="active">Order List</li>    
@endsection
@section('content')
    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Order List</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Order ID</th>
                                <th>Invoice</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data->count()>0)
                                @foreach($data as $key=>$value)  
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->order_id }}</td>
                                        <td>{{ $value->invoice }}</td>
                                        <td>{{ date('d-m-Y',strtotime($value->order_date)) }}</td>
                                        <td>
                                            <!-- <a class="openmodal" data-id="{{ $value->order_id }}" data-invoice="{{ $value->invoice }}">Action</a> -->
                                            <a href="{{ route('order-status',$value->order_id) }}" class="btn btn-primary" target="_blank">See Details</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Order Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body" style='overflow:auto;height:450px;'>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Customer Name - </strong><span id="customer_name"></span>
                                </div>
                                <div class="col-md-6">
                                    <strong>Order Invoice - </strong><span id="order_invoice"></span>
                                </div>
                                <div class="col-md-6">
                                    <strong>Mobile No - </strong><span id="mobile_no"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Product Details</h4>
                                    <div class="table-responsive"> 
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>#</td>
                                                <td>Product</td>
                                                <td>Product Status</td>
                                                <td>Product Sub Status</td>
                                                <td>PO No./PO Date</td>
                                            </tr>
                                        </thead>
                                        <tbody id="productdetails"></tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Interaction Details</h4> 
                                    <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>#</td>
                                                <td>Created Date</td>
                                                <td>Created By</td>
                                                <td>Role</td>
                                                <td>Product Status</td>
                                                <td>Product Sub Status</td>
                                                <td>Remarks</td>
                                            </tr>
                                        </thead>
                                        <tbody id="interaction"></tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>  
    </div>
@endsection    
@section('page_level_js')
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        var callstatus = {!! json_encode(config('custom.ecommerce_status')) !!};
        var callsubstatus = {!! json_encode(config('custom.ecommerce_sub_status')) !!};
        var product = [];
        jQuery(document).ready(function () {
            
        })
        $('.openmodal').click(function(){
            $('#frm :input').not('#ecommerce_status,#token,.meet').val('');
            $('input[type=radio]').prop('checked',false)
            var id = $(this).data('id');
            var invoice = $(this).data('invoice');
            $('#disporderno').html(id);
            $('#dispinvoiceno').html(invoice);
             $('#interaction,#productdetails').html('');
            $.ajax({
                url: "{{ route('product-status') }}",
                data:{'order_no':id},  
                type: 'get',
                async:true,
                cache: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success:function(response){
                    var append="";
                    $('span#order_invoice').text(response['order']['invoice']);
                    $('span#customer_name').text(response['order']['customer']['first_name'].toUpperCase()+' '+response['order']['customer']['last_name'].toUpperCase());
                    $('span#mobile_no').text(response['order']['customer']['phone_no']);
                    $.each(response['product'],function(index,item){
                        var callst = (parseInt(item['ecommerce_status'])===0) ? "-" : callstatus[item['ecommerce_status']][item['ecommerce_status']];
                        var callsubst = (item['ecommerce_sub_status']===null) ? "-" : callsubstatus[item['ecommerce_status']][item['ecommerce_sub_status']];
                        append+='<tr><td>'+(index+1)+'</td>';
                        append+='<td>'+(item['product_sku'])+'</td>';
                        append+='<td>'+(callst)+'</td>';
                        append+='<td>'+(callsubst)+'</td>';
                        if(item['po_number'] != '' && item['po_number'] != null) {
                            append+='<td>'+(item['po_number'])+'/'+(item['po_date'])+'</td></tr>';
                        } else {
                            append+='<td>-</td></tr>';
                        }
                        product[item['cart_id']] = item['product_sku'];
                    });
                    $('#productdetails').append(append);
                    var append="";
                    $.each(response['interaction'],function(index,item){
                        append+='<tr><td colspan=6>'+(product[index])+'</td></tr>';
                        $.each(item,function(index1,item1){
                            append+='<tr><td>'+(index1+1)+'</td>';
                            append+='<td>'+(item1['int_date'])+'</td>';
                            append+='<td>'+(item1['name'])+'</td>';
                            append+='<td>'+(item1['role'])+'</td>';
                            append+='<td>'+(callstatus[item1['ecommerce_status']][item1['ecommerce_status']])+'</td>';
                            append+='<td>'+(callsubstatus[item1['ecommerce_status']][item1['ecommerce_sub_status']])+'</td>';
                            append+='<td>'+(item1['remarks'])+'</td></tr>';

                        });
                    });
                    $('#interaction').append(append);
                    $('#modal').modal('show');
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
            
        });
    </script>
@endsection
