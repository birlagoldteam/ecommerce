@extends('layouts.app')
@section('page_title')
    Vendor Settlement List
@endsection
@section('page_level_style_top')
    
@endsection
@section('breadcrumb')
    <li class="active">Vendor Settlement List</li>    
@endsection
@section('content')
    <div class="row">
       <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Vendor Settlement List</div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Order ID</th>
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th>Vendor</th>
                                <th>Vendor Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data->count()>0)
                                @foreach($data as $key=>$value)  
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->order_id }}</td>
                                        <td>{{ $value->product->product_code }}</td>
                                        <td>{{ $value->product->product_name }}</td>
                                        <td>{{ $value->vendor->vendor_code }}</td>
                                        <td>{{ $value->vendor->Company_name }}</td>
                                        <td><a href="{{ route('vendor-settlement',[$value->cart_id]) }}">Action</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection    
@section('page_level_js')
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            FormValidation.init();
        });
        function formsubmit(form){
            form.submit();
        }
        $('#accountReceivedForm').submit(function(){
            $('#loading').show();
        });
    </script>
@endsection