@extends('layouts.app')
@section('page_title')
    Vendor Settlement List
@endsection
@section('page_level_style_top')
    
@endsection
@section('breadcrumb')
    <li class="active">Vendor Settlement List</li>    
@endsection
@section('content')
    <div class="row">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Vendor Settlement List</div>
            </div>
            <div class="portlet-body">
                 <form method="post" action="{{ route('vendor-settlement-save') }}" id="vendorsettlememnt" name="vendorsettlememnt">
                    <input type="hidden" name="tax_rate" id="tax_rate">
                    <input type="hidden" name="subtotal" id="subtotal">
                    <input type="hidden" name="order_id" id="order_id">
                    <input type="hidden" name="cart_id" id="cart_id">
                    <input type="hidden" name="wastagegold" id="wastagegold">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label">Order No :</label>
                            <div class="col-md-3">{{ $data->order_id }}</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label">Product Code:</label>
                            <div class="col-md-9">{{ $data->product->product_code }} ({{ $data->product->product_name }})</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label">Vendor Name :</label>
                            <div class="col-md-9">{{ $data->vendor->Company_name }} ({{ $data->vendor->vendor_code }})</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label">Settlement Type :</label>
                            <div class="col-md-3">
                                <select class="form-control" name="settlement_type" id="settlement_type">
                                    <option value="">Select</option>
                                    @foreach($settlementData as $key=>$value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="portlet box blue showhide">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-comments"></i><span></span></div>
                        </div>
                        <div class="portlet-body"> 
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 control-label">Invoice / Receipt No:</label>
                                    <div class="col-md-3">
                                        <input type="text" name="invoice_no" id="invoice_no" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 control-label">Product Weight (22K):</label>
                                    <div class="col-md-2">
                                        <input type="text" name="product_weight" id="product_weight" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group 1 checkshowhide">
                                <div class="row">
                                    <label class="col-md-3 control-label">Rate:</label>
                                    <div class="col-md-2">
                                        <input type="text" name="rate" id="rate" class="form-control">
                                    </div>
                                    <div class="col-md-1 subtotal"></div>
                                </div>
                            </div>
                            <div class="form-group 1 checkshowhide">
                                <div class="row">
                                    <label class="col-md-3 control-label">Tax:</label>
                                    <div class="col-md-2">
                                        <input type="text" name="tax" id="tax" class="form-control" readonly>
                                    </div>
                                    <div class="col-md-1 tax_rate"></div>
                                </div>
                            </div>
                            <div class="form-group 1 checkshowhide">
                                <div class="row">
                                    <label class="col-md-3 control-label">Total:</label>
                                    <div class="col-md-3">
                                        <input type="text" name="total" id="total" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group 1 checkshowhide">
                                <div class="row">
                                    <label class="col-md-3 control-label">Settlement Amount:</label>
                                    <div class="col-md-3">
                                        <input type="text" name="settlement" id="settlement" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group 3 checkshowhide">
                                <div class="row">
                                    <label class="col-md-3 control-label">Wastage (%):</label>
                                    <div class="col-md-1">
                                        <input type="text" name="wastagepercent" id="wastagepercent" class="form-control">
                                    </div>
                                    <div class="col-md-2" style="margin-top: 9px;"><span class="wastagegold"></span> (22K)</div>
                                </div>
                            </div>
                            <div class="form-group 3 checkshowhide">
                                <div class="row">
                                    <label class="col-md-3 control-label">Gold (24K) :</label>
                                    <div class="col-md-2">
                                        <input type="text" name="totalwastagegold" id="totalwastagegold" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group 3 checkshowhide">
                                <div class="row">
                                    <label class="col-md-3 control-label">Gold Settle:</label>
                                    <div class="col-md-2">
                                        <input type="text" name="wastagegoldsettle" id="wastagegoldsettle" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-1">
                                        <input type="submit" id="savebtn" value="Save" class="form-control btn green">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection    
@section('page_level_js')
    <!--script src="{{ asset('public/validation/form-validation.js') }}"></script>
    <script src="{{ asset('public/validation/jquery.validate.js') }}"></script-->
    <script type="text/javascript">
        var taxper = 0;
        var taxpercenatage = {!!  json_encode(config('custom.taxrate')) !!};
        jQuery(document).ready(function () {
            App.init();
            FormValidation.init();
            $('.showhide').hide();
        });
        $('#settlement_type').change(function(){
            $("#vendorsettlememnt").validate().resetForm();
            $('.has-error').removeClass('has-error');
            var val = $(this).val();
            $('.showhide :input').not('#savebtn').val('');
            $('.checkshowhide').hide();
            if(val!=""){
                $('.showhide').show();
                $('.'+val).show();
            }else{
                $('.showhide').hide();
            }
            var taxval = taxrate(val)
            $('.tax_rate').html(taxval+' %');
            taxper = taxval;
        });

        $('#product_weight,#rate,#wastagepercent').on('keyup',function(){
            var weight = parseFloat($('#product_weight').val() || 0);
            var rate = parseFloat($('#rate').val() || 0);
            var wastagepercent = parseFloat($('#wastagepercent').val() || 0);
            var type = $('#settlement_type option:selected').val();
            var subtotal = 0;
            $('.subtotal').html(0);
            if(weight!=0 && rate!=0 && type==1){
                subtotal = parseFloat((rate*weight).toFixed(3));
                $('.subtotal').html(subtotal);
                var taxrate = parseFloat(((taxper*subtotal)/100).toFixed(3));
                $('#tax').val(taxrate);
                var total = subtotal+taxrate;
                $('#total').val(total.toFixed(3));
            }else if(type==3 && wastagepercent!=0 && weight!=0){
                var productperwastage = parseFloat(((weight*wastagepercent)/100)+weight).toFixed(3);
                $('.wastagegold').html(productperwastage);
                var goldmetalsettle = ((productperwastage*92)/99.5).toFixed(3);
                $('#totalwastagegold').val(goldmetalsettle);
            }
        });

        function formsubmit(obj){
            $('#subtotal').val($('.subtotal').html());
            $('#tax_rate').val(taxrate(""));
            $('#order_id').val("{{ $data->order_id }}");
            $('#cart_id').val("{{  $data->cart_id }}");
            $('#wastagegold').val($('.wastagegold').html());
            obj.submit();
            return true;    
        }

        var taxrate = function(option){
            var  value = (option=="") ? $('#settlement_type option:selected').val() : option;
            return taxpercenatage[value]==undefined ? 0 : taxpercenatage[value];
        };
    </script>
@endsection