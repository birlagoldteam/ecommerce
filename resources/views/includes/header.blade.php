<div class="clearfix navbar-fixed-top">
   <!--  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="toggle-icon">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </span>
    </button> -->
    <a id="index" class="page-logo" href="{{ route('dashboard-url') }}">
        <img src="{{ asset('public/img/logo.png') }}" alt="Logo"> 
    </a>
    <div class="topbar-actions">
        <div class="btn-group-img btn-group">
            <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <span>{{ Auth::check()===true ? Auth::user()->name : ""  }}</span>
            </button>
            <ul class="dropdown-menu-v2" role="menu">
                <li>
                    <a href="{{ route('logout') }}">
                    <i class="icon-key"></i> Log Out </a>
                </li>
            </ul>
        </div>
    </div>
</div>