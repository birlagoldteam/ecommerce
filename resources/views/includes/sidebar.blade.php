<?php
$obj = request();
$route = Route::currentRouteName();
$firstRouteParameter = $obj->segment(2);
$secondRouteParameter = $obj->segment(3);
?>
<ul class="nav navbar-nav">
    @if(in_array(Auth::user()->role,['Operations Head','Operations']))
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ ($route=="operation-dashboard") ? "active open" : ""  }}">
            <a href="{{ route('dashboard-url') }}" class="text-uppercase"><i class="icon-briefcase"></i>Dashboard</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('dashboard-url') }}">Dashboard</a>
                </li>
            </ul>
        </li>
         <li class="dropdown dropdown-fw dropdown-fw-disabled {{ ($route=="order-details" && ((empty($firstRouteParameter) && empty($secondRouteParameter)) || ($firstRouteParameter==1 && empty($secondRouteParameter)))) ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase">
                <i class="icon-briefcase"></i> Order </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('order-details') }}">Total Order</a>
                </li>
                <li>
                    <a href="{{ route('order-details',[1]) }}">Successfull Orders</a>
                </li>
                <li>
                    <a href="#">Failed Orders</a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ ($route=="order-details" && (($firstRouteParameter==2 && empty($secondRouteParameter)) || ($firstRouteParameter==1 && $secondRouteParameter=="pending") || ($firstRouteParameter==4 && empty($secondRouteParameter)) )) ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase">
                <i class="icon-briefcase"></i>Merchandiser Order</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('order-details',[2]) }}">Order Placed</a>
                </li>
                <li>
                    <a href="{{ route('order-details',[1,'pending']) }}">Order Pending</a>
                </li>
                <li>
                    <a href="{{ route('order-details',[4]) }}">QC Done Product</a>
                </li>
                <li>
                    <a href="javascript:;">QC Rejected Product</a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled  {{ ($route=="order-details" && (($firstRouteParameter==4 && $secondRouteParameter=="pending") || ($firstRouteParameter==5 && empty($secondRouteParameter)))) ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase"><i class="icon-briefcase"></i>TAX INVOICE</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('order-details',[5]) }}"> Tax Invoice Generated </a>
                </li>
                <li> 
                    <a href="{{ route('order-details',[4,'pending']) }}">Tax Invoice Pending</a>
                </li>            
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled  {{ (($route=="operation-dispatch") || ($route=="operation-dispatch-status")  || ($route=="order-details" && (($firstRouteParameter==6 && empty($secondRouteParameter)) || ($firstRouteParameter==7 && empty($secondRouteParameter)) ))) ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase"><i class="icon-briefcase"></i>Shipment</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('operation-dispatch') }}">Ready for Shipmnet</a>
                </li>
                <li>
                    <a href="{{ route('order-details',[6]) }}">Order Shipped</a>
                </li>
                <li>
                    <a href="{{ route('operation-dispatch-status') }}">Shipment Status</a>
                </li>
                <li>
                    <a href="{{ route('order-details',[7]) }}"> Shipment Successful </a>
                </li>
                <li>
                    <a href="javascript:;">Shipment Failed</a>
                </li>            
            </ul>
        </li>
    @endif
    @if(in_array(Auth::user()->role,['Accounts']))
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ ($route=="accounts-dashboard") ? "active open" : ""  }}">
            <a href="{{ route('dashboard-url') }}" class="text-uppercase"><i class="icon-briefcase"></i>Dashboard</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('dashboard-url') }}">Dashboard</a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ ($route=="account" ||($route=="order-details" && ((empty($firstRouteParameter) && empty($secondRouteParameter)) || ($firstRouteParameter==1 && empty($secondRouteParameter))))) ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase">
                <i class="icon-briefcase"></i> Order </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('account') }}">Receive Payment</a>
                </li>
                <li>
                    <a href="{{ route('order-details') }}">Total Order</a>
                </li>
                <li>
                    <a href="{{ route('order-details',[1]) }}">Payment Received</a>
                </li>
                <li>
                    <a href="javascript:;">Payment Failed</a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ ($route=="merchandiser-dashboard-data") ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase">
                <i class="icon-briefcase"></i>Order Placed</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('merchandiser-dashboard-data',[2]) }}">Product's Order Placed to Vendor</a>
                </li>
                <li>
                     <a href="{{ route('merchandiser-dashboard-data',[3]) }}">Product Received Form Vendor </a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled  {{ (($route=="order-details" && ($firstRouteParameter==5 && empty($secondRouteParameter)))  || $route=="final-billing-orders") ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase"><i class="icon-briefcase"></i>TAX INVOICE</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('order-details',[5]) }}"> Tax Invoice Generated </a>
                </li>
                <li> 
                    <a href="{{ route('final-billing-orders') }}">Tax Invoice Pending</a>
                </li>            
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled">
            <a href="javascript:;" class="text-uppercase"><i class="icon-briefcase"></i>Vendor Settlement</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="javascript:;">Vendor Settlement</a>
                </li>
                <li> 
                    <a href="javascript:;">Vendor Settlement Pending</a>
                </li>            
            </ul>
        </li>
    @endif
    @if(in_array(Auth::user()->role,['Merchandiser','Merchandiser Head']))
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ ($route=="merchandiser-dashboard") ? "active open" : ""  }}">
            <a href="{{ route('dashboard-url') }}" class="text-uppercase"><i class="icon-briefcase"></i>Dashboard</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('dashboard-url') }}">Dashboard</a>
                </li>
            </ul>
        </li>
         <li class="dropdown dropdown-fw dropdown-fw-disabled {{ (($route=="merchandiser-received" && (($firstRouteParameter==2 && $secondRouteParameter=="6") || (empty($firstRouteParameter) && empty($secondRouteParameter)))) || ($route=="merchandiser-dashboard-data" && (($firstRouteParameter==3 && empty($secondRouteParameter)) || ($firstRouteParameter==2 && ($secondRouteParameter==1)) ))) ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase">
                <i class="icon-briefcase"></i> Order </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('merchandiser-dashboard-data',[3]) }}">Received Products From Vendors</a>
                </li>
                <li>
                    <a href="{{ route('merchandiser-dashboard-data',[2,1,'pending']) }}">Pending Products Received From Vendor</a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ (($route=="merchandiser-dashboard-data") && (($firstRouteParameter==4 && empty($secondRouteParameter)) || ($firstRouteParameter==3 && ($secondRouteParameter==1)) )) ? "active open" : ""  }}">
            <a href="javascript:;" class="text-uppercase">
                <i class="icon-briefcase"></i>QC Details</a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li>
                    <a href="{{ route('merchandiser-dashboard-data',[4]) }}">Product QC Done</a>
                </li>
                <li>
                    <a href="{{ route('merchandiser-dashboard-data',[3,1,'pending']) }}">Product QC Pending </a>
                </li>
                <li>
                    <a href="javascript:;">QC Rejected Products </a>
                </li> 
            </ul>
        </li>
    @endif
</ul>