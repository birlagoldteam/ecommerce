<?php

Route::get('/{name?}','LoginController@index')->name('login')->where('name', '(login)?');

Route::get('/logout',[
    'uses' => 'LoginController@logout',
     'as' => 'logout'
]);

Route::get('/dashboard', [
    'uses' => 'DashboardController@index',
    'middleware'=>'auth',
    'as' => 'dashboards'
]);

Route::get('/dashboard-url', [
    'uses' => 'LoginController@redirectUrl',
    'middleware'=>'auth',
    'as' => 'dashboard-url'
]);


Route::get('/accounts-order', [
    'uses' => 'EcommerceController@accountOrders',
    'middleware'=>'auth',
    'as' => 'account'
]);

Route::post('/save-accounts-order', [
    'uses' => 'EcommerceController@updateAccountStatus',
    'middleware'=>'auth',
    'as' => 'save-account'
]);

Route::get('/merchandiser-order', [
    'uses' => 'EcommerceController@merchandiserOrders',
    'middleware'=>'auth',
    'as' => 'merchandiser'
]);

Route::post('/save-merchandiser-order', [
    'uses' => 'EcommerceController@updateMerchandiserStatus',
    'middleware'=>'auth',
    'as' => 'save-merchandiser'
]);

Route::get('/merchandiser-received-order/{orderid?}/{product?}', [
    'uses' => 'EcommerceController@merchandiserReceivedOrder',
    'middleware'=>'auth',
    'as' => 'merchandiser-received'
]);

Route::post('/save-merchandiser-received-order', [
    'uses' => 'EcommerceController@updateMerchandiserOrderReceived',
    'middleware'=>'auth',
    'as' => 'merchandiser-received-order'
]);


Route::get('/merchandiser-qc-order/{orderid?}/{product?}', [
    'uses' => 'EcommerceController@merchandiserQcOrders',
    'middleware'=>'auth',
    'as' => 'merchandiser-qc'
]);

Route::post('/save-merchandiser-qc-order', [
    'uses' => 'EcommerceController@updateMerchandiserQcOrders',
    'middleware'=>'auth',
    'as' => 'save-qc-merchandiser'
]);

Route::get('/get-porduct-details', [
    'uses' => 'EcommerceController@productdetails',
    'middleware'=>'auth',
    'as' => 'porduct-details'
]);

Route::get('/operation-dispatch', [
    'uses' => 'EcommerceController@operationDispatch',
    'middleware'=>'auth',
    'as' => 'operation-dispatch'
]);

Route::post('/save-operation-dispatch', [
    'uses' => 'EcommerceController@saveOperationDispatch',
    'middleware'=>'auth',
    'as' => 'save-operation-dispatch'
]);

Route::get('/operation-dispatch-status', [
    'uses' => 'EcommerceController@operationDispatchStatus',
    'middleware'=>'auth',
    'as' => 'operation-dispatch-status'
]);

Route::post('/save-operation-dispatch-status', [
    'uses' => 'EcommerceController@saveOperationDispatchStatus',
    'middleware'=>'auth',
    'as' => 'save-operation-dispatch-status'
]);

Route::get('/final-billing-orders', [
    'uses' => 'EcommerceController@finalBillingOrders',
    'middleware'=>'auth',
    'as' => 'final-billing-orders'
]);

Route::get('/generate-invoice/{id}', [
    'uses' => 'EcommerceController@generateInvoice',
    'middleware'=>'auth',
    'as' => 'generate-invoice'
]);

Route::get('/invoice-generated', [
    'uses' => 'EcommerceController@invoiceGenerated',
    'middleware'=>'auth',
    'as' => 'invoice-generated'
]);

Route::get('/check-invoice', [
    'uses' => 'EcommerceController@checkInvoiceGenerated',
    'middleware'=>'auth',
    'as' => 'check-invoice'
]);

Route::get('/operations-dashboard', [
    'uses' => 'DashboardController@operactionDashboard',
    'middleware'=>'auth',
    'as' => 'operation-dashboard'
]);

Route::get('/merchandiser-dashboard', [
    'uses' => 'DashboardController@merchandiserdashboard',
    'middleware'=>'auth',
    'as' => 'merchandiser-dashboard'
]);

Route::get('/merchandiser-dashboard-data/{status?}/{substatus?}/{flag?}', [
    'uses' => 'DashboardController@merchandiserDashboardData',
    'middleware'=>'auth',
    'as' => 'merchandiser-dashboard-data'
]);

Route::get('/accounts-dashboard', [
    'uses' => 'DashboardController@accountdashboard',
    'middleware'=>'auth',
    'as' => 'accounts-dashboard'
]);

Route::get('/order-details/{value?}/{pending?}', [
    'uses' => 'ReportController@index',
    'middleware'=>'auth',
    'as' => 'order-details'
]);

Route::get('/product-status', [
    'uses' => 'ReportController@productWithStatus',
    'middleware'=>'auth',
    'as' => 'product-status'
]);

Route::get('/order-status/{id}', [
    'uses' => 'ReportController@orderStatus',
    'middleware'=>'auth',
    'as' => 'order-status'
]);

Route::get('/vendor-settlement-list', [
    'uses' => 'VendorSettlementController@index',
    'middleware'=>'auth',
    'as' => 'vendor-settlement-list'
]);

Route::get('/vendor-settlement/{id}', [
    'uses' => 'VendorSettlementController@vendorsettlement',
    'middleware'=>'auth',
    'as' => 'vendor-settlement'
]);

Route::post('/vendor-settlement-save', [
    'uses' => 'VendorSettlementController@vendorsettlementsave',
    'middleware'=>'auth',
    'as' => 'vendor-settlement-save'
]);



Route::get('check',function(){
    dd(request()->session()->has('invoiceGenerated'));
    
});