<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Orders;
use App\ShopingCart;
use DB;

class DashboardController extends Controller
{
	private $crm_database;
    private $database2;

    public function __construct(){
		$this->crm_database=env("DB_DATABASE");
		$this->database2=env("DB_DATABASE2");
	}

    /*public function index(){
        $order = Orders::where('ecommerce_status',4);
        if(Auth::user()->role == 'Admin') {
            return view('dashboard.dashboard');
        } else {
            return view('dashboard.accounts-dashboard');
        }
    }*/
    
    public function merchandiserdashboard(Request $request){

        $database=env("DB_DATABASE2");

        $totalOrder = Orders::where('pay_received','1')
                            ->count('order_id');

        $placedOrder =  Orders::where('ecommerce_status','>=',2)
                              ->select(DB::raw("count(order_id) as cnt"))
                              ->first();                      

        
        $pendingOrder =  Orders::where('ecommerce_status','=',1)
                               ->select(DB::raw("count(order_id) as cnt"))
                               ->first();  

        $receivedProductFromVendor = ShopingCart::where(function($q) use($database){
                                            $q->where("$database.sha_shoppingcarts.ecommerce_status",'=','3');
                                            $q->where("$database.sha_shoppingcarts.ecommerce_sub_status",'=','1');
                                            $q->orWhere(function($q1) use($database){
                                                $q1->orWhere("$database.sha_shoppingcarts.ecommerce_status",'>=','4');
                                            });
                                        })   
                                        ->select(DB::raw("count(cart_id) as cnt"))               
                                        ->first();

        $pendingProductReceivedFromVendor = ShopingCart::where("sha_shoppingcarts.ecommerce_status",'=','2')
                                        ->where("sha_shoppingcarts.ecommerce_sub_status",'=','1')
                                        ->select(DB::raw("count(cart_id) as cnt"))               
                                        ->first();

        $productQcDone = ShopingCart::where(function($q) use($database){
                                $q->where("$database.sha_shoppingcarts.ecommerce_status",'=','4');
                                $q->where("$database.sha_shoppingcarts.ecommerce_sub_status",'=','1');
                                $q->orWhere(function($q1) use($database){
                                    $q1->orWhere("$database.sha_shoppingcarts.ecommerce_status",'>=','5');
                                });
                          })     
                          ->select(DB::raw("count(cart_id) as cnt"))               
                          ->first();                           

        $productQcPending = ShopingCart::where("sha_shoppingcarts.ecommerce_status",'=','3')
                                        ->where("sha_shoppingcarts.ecommerce_sub_status",'=','1')
                                        ->select(DB::raw("count(cart_id) as cnt"))               
                                        ->first();

       $data = ['totalOrder'=>$totalOrder,
                'placedOrder'=>$placedOrder,
                'pendingOrder'=>$pendingOrder,
                'receivedProductFromVendor'=>$receivedProductFromVendor,
                'pendingProductReceivedFromVendor'=>$pendingProductReceivedFromVendor,
                'productQcDone'=>$productQcDone,
                'productQcPending'=>$productQcPending];

        return view('dashboard.merchandisers-dashboard',$data);
      }



    public function accountdashboard() {
    	$crm_database = $this->crm_database;
        $database = $this->database2;

       	$obj = Orders::whereIn('status',['payment_received','offline_payment_requested']);

        $obj1 = clone($obj);
        $totalOrders = $obj1->select(DB::raw("count(order_id) as ordercount"),DB::raw("sum(order_amount) as orderamount"))
                             ->first();

        $obj2 = clone($obj);
        $orderFailed = $obj2->where('pay_received',2)
                            ->select(DB::raw("count(order_id) as ordercount"),DB::raw("sum(order_amount) as orderamount"))
                            ->first();

        $obj3 = clone($obj);
        $ordersSuccess = $obj3->where('pay_received',1)
                              ->select(DB::raw("count(order_id) as ordercount"),DB::raw("sum(order_amount) as orderamount"))
                              ->first();

        $obj4 = clone($obj);
        $ordersInvoicePending = $obj4->where('ecommerce_status',4)
                                     ->select(DB::raw("count(order_id) as ordercount"))
                                     ->first();

        $obj5 = clone($obj);
        $ordersInvoiceGenerated = $obj5->where('ecommerce_status','>=',5)
                                       ->select(DB::raw("count(order_id) as ordercount"))
                                       ->first();

        $orderPlacedVendor = ShopingCart::where('ecommerce_status','>=',2)
                                        ->select(DB::raw("count(cart_id) as ordercount"))
                                        ->first(); 

        $orderReceivedVendor = ShopingCart::where('ecommerce_status','>=',3)
                                          ->select(DB::raw("count(cart_id) as ordercount"))
                                          ->first();

        $orderVendorSettlement = $this->vendotSettlement(1)
                                      ->select(DB::raw("count(cart_id) as ordercount"))
                                       ->first();

        $orderVendorSettlementPending = $this->vendotSettlement(0)
                                             ->select(DB::raw("count(cart_id) as ordercount"))
                                             ->first();
        
        $data = [
            'totalOrders' =>$totalOrders,
            'orderFailed'=>$orderFailed,
            'ordersSuccess'=>$ordersSuccess,
            'ordersInvoicePending'=>$ordersInvoicePending,
            'ordersInvoiceGenerated'=>$ordersInvoiceGenerated,
            'orderPlacedVendor'=>$orderPlacedVendor,
            'orderReceivedVendor'=>$orderReceivedVendor,
            'orderVendorSettlement'=>$orderVendorSettlement,
            'orderVendorSettlementPending'=>$orderVendorSettlementPending
        ];
        
        return view('dashboard.accounts-dashboard')->with($data);	
    }

    public function operactionDashboard(){
        $crm_database = $this->crm_database;
        $database = $this->database2;

        $obj = Orders::whereIn('status',['payment_received','offline_payment_requested']);

        $orderGenerate = $obj->select(DB::raw("count(order_id) as ordercount"),DB::raw("sum(order_amount) as orderamount"))
                             ->first();

        $orderFailed = Orders::where('pay_received',2)
                             ->select(DB::raw("count(order_id) as ordercount"))
                             ->first();

        $orderReceived = $obj->where('pay_received',1)
                             ->select(DB::raw("count(order_id) as ordercount"))
                             ->first();
                             
        /*$orderQcDone = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when ((ecommerce_status = 4 and ecommerce_sub_status = 1) Or (ecommerce_status >= 5))then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();*/
        $orderQcDone = Orders::where('ecommerce_status','>=',4)
                             ->select(DB::raw("count(order_id) as cnt"))
                             ->first();

        /*$orderInvoice = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when ((ecommerce_status = 5 and ecommerce_sub_status = 1) Or (ecommerce_status >=6))then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();*/  

        $orderInvoice = Orders::where('ecommerce_status','>=',5)
                              ->select(DB::raw("count(order_id) as cnt"))
                              ->first();

        /*$orderPlaced = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when ((ecommerce_status = 2 and ecommerce_sub_status = 1) Or (ecommerce_status >=3))then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();*/

        $orderPlaced = Orders::where('ecommerce_status','>=',2)
                              ->select(DB::raw("count(order_id) as cnt"))
                              ->first();

        /*$orderPending = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 1 and ecommerce_sub_status = 1)then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue>0) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();*/

        $orderPending = Orders::where('ecommerce_status','=',1)
                              ->select(DB::raw("count(order_id) as cnt"))
                              ->first();

        $orderQcRejected = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 4 and ecommerce_sub_status = 2)then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue>0) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();
        
        /*$orderInvoicePending = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 4 and ecommerce_sub_status = 1)then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();*/

        $orderInvoicePending = Orders::where('ecommerce_status','=',4)
                                     ->select(DB::raw("count(order_id) as cnt"))
                                     ->first();

        /*$orderReadyShipment = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 5 and ecommerce_sub_status = 1)then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();*/

        $orderReadyShipment = Orders::where('ecommerce_status','=',5)
                                    ->select(DB::raw("count(order_id) as cnt"))
                                    ->first();

        /*$orderShipped = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when ((ecommerce_status = 6 and ecommerce_sub_status = 1) Or (ecommerce_status >=7))then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();*/

        $orderShipped = Orders::where('ecommerce_status','>=',6)
                              ->select(DB::raw("count(order_id) as cnt"))
                              ->first();


        $orderShippedFailed = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 7 and ecommerce_sub_status = 2)then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();

        $orderShippedSuccess = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 7 and ecommerce_sub_status = 1)then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue=cnt) as checkorder"),function($join){
                $join->on("sha_orders.order_id",'=','checkorder.orderval');
            })
            ->select(DB::raw("count(cnt) as cnt"))
            ->first();



        $data = [
            'orderGenerate'=>$orderGenerate,
            'orderFailed'=>$orderFailed,
            'orderReceived'=>$orderReceived,
            'orderQcDone'=>$orderQcDone,
            'orderInvoice'=>$orderInvoice,
            'orderPlaced' =>$orderPlaced,
            'orderPending' => $orderPending,
            'orderQcRejected'=>$orderQcRejected,
            'orderInvoicePending'=>$orderInvoicePending,
            'orderReadyShipment'=>$orderReadyShipment,
            'orderShipped' =>$orderShipped,
            'orderShippedFailed'=>$orderShippedFailed,
            'orderShippedSuccess' =>$orderShippedSuccess
        ];
        return view('dashboard.operations-dashboard',$data); 
    }

    public function merchandiserDashboardData($status=null,$substatus=null,$flag=null){
        //$this->crm_database=env("DB_DATABASE");
        $database = $this->database2;
        $data = Orders::join("$database.sha_shoppingcarts","sha_shoppingcarts.order_id",'=','sha_orders.order_id')
                       ->join("$database.sha_products","$database.sha_shoppingcarts.product_id","=","$database.sha_products.product_id")
                       ->where(function($q) use ($flag,$status,$substatus){
                            if($flag=="pending"){
                                $q->where("sha_shoppingcarts.ecommerce_status",'=',$status);
                                $q->where("sha_shoppingcarts.ecommerce_sub_status",'=',$substatus);
                            }else{
                                $q->where('sha_shoppingcarts.ecommerce_status','>=',$status);
                            }
                        })
                       ->select("$database.sha_shoppingcarts.order_id","$database.sha_orders.invoice","$database.sha_orders.order_date","$database.sha_products.product_sku","$database.sha_shoppingcarts.cart_id")
                       ->get();
        return view('dashboard.merchandiser-data',['data'=>$data]);
    }

    private function datafailure($status,$substatus){
        return Orders::join(DB::raw("(SELECT sum(case when (ecommerce_status = $status and ecommerce_sub_status = $substatus)then 1 else 0 end) as checkvalue,order_id as orderval FROM sha_shoppingcarts group by order_id having checkvalue>0) as checkorder"),function($join){
                $join->on("sha_orders.order_id", '=', 'checkorder.orderval');
            });
            
    }
}
