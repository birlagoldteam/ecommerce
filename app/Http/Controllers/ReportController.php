<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Orders;
use App\ShopingCart;
use App\EcommerceInteraction;
use DB;

class ReportController extends Controller{

	public function index($value=null,$pending=null){
		$data = Orders::whereIn('status',['payment_received','offline_payment_requested'])
					 ->where(function($obj) use ($value,$pending){
					 	if(!empty($value)){
					 		if($pending=="pending"){
					 			$obj->where('ecommerce_status','=',$value);
					 		}else{
					 			$obj->where('ecommerce_status','>=',$value);
					 		}
					 	}
					 })
				     ->select(["order_date","order_id","invoice","order_amount","amount_received"])
				     ->get();
		return view('report.index',['data'=>$data]);
	}

	public function productWithStatus(){
		
		$order_id = request()->order_no;
		$database=env("DB_DATABASE2");
		$data ['order'] = Orders::where('order_id','=',$order_id)->with('customer')->first();
		$data ['product'] = ShopingCart::join("$database.sha_products","$database.sha_shoppingcarts.product_id","=","$database.sha_products.product_id")
									  ->leftjoin("$database.sha_vendor","$database.sha_vendor.vendor_id","=","$database.sha_products.vendor_id")
				  					  ->where('order_id',$order_id)
				  					  ->select("ecommerce_status","ecommerce_sub_status","product_sku","Company_name","processing_date","check_certificate","check_image","check_size","check_weight","check_finishing","check_diamaondquality","cart_id","po_number","po_date")
				  					  ->get();
		$database1=env("DB_DATABASE");
		$interaction = EcommerceInteraction::join("$database1.users","$database1.users.id","=","$database1.ecommerce_interaction.user_id")
										   ->where('order_id',$order_id)
										   ->select("$database1.ecommerce_interaction.*","$database1.users.name",DB::raw("DATE_FORMAT($database1.ecommerce_interaction.created_at,'%d-%m-%Y') as int_date"))
											->get();
		$interactiondata = json_decode(json_encode($interaction),1);
		foreach ($interactiondata as $key => $value) {
			$data['interaction'][$value['shoppingcart_id']][]=$value;
		}
		return $data;
	}

	public function orderStatus($id) {
		$order_id = $id;
		$database=env("DB_DATABASE2");
		$data ['order'] = Orders::where('order_id','=',$order_id)->with('customer')->first();
		$data ['product'] = ShopingCart::join("$database.sha_products","$database.sha_shoppingcarts.product_id","=","$database.sha_products.product_id")
									  ->leftjoin("$database.sha_vendor","$database.sha_vendor.vendor_id","=","$database.sha_products.vendor_id")
				  					  ->where('order_id',$order_id)
				  					  ->select("$database.sha_shoppingcarts.*","product_sku")
				  					  ->get();
		$database1=env("DB_DATABASE");
		$interaction = EcommerceInteraction::join("$database1.users","$database1.users.id","=","$database1.ecommerce_interaction.user_id")
										   ->where('order_id',$order_id)
										   ->select("$database1.ecommerce_interaction.*","$database1.users.name",DB::raw("DATE_FORMAT($database1.ecommerce_interaction.created_at,'%d-%m-%Y') as int_date"))
											->get();
		$interactiondata = json_decode(json_encode($interaction),1);
		$data['interaction'] = [];
		foreach ($interactiondata as $key => $value) {
			$data['interaction'][$value['shoppingcart_id']][]=$value;
		}
		return view('report.order_status',['data'=>$data]);
	}
}
