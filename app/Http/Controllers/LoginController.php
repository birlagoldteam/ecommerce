<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;

class LoginController extends Controller{

	public function index(Request $request){
		if(Auth::guard('web')->check()===true){
			return $this->redirectUrl();
		}
		$user_id = json_decode(base64_decode($request->id),1);
		if(isset($user_id['id'])){
			Auth::loginUsingId($user_id['id']);
			return $this->redirectUrl();
		}
		return view('users.invalidlogin');
	}

	public function logout(Request $request){
		Auth::logout();
        $request->session()->flush();
        return $this->index($request);
	}

	public function redirectUrl(){
		$role = Auth::user()->role;
		if($role=="Accounts")
			return redirect()->route('accounts-dashboard');
		elseif($role=="Customer Service")
			return redirect()->route('dashboard');
		elseif(in_array($role,["Operations",'Operations Head']))
			return redirect()->route('operation-dashboard');
		elseif(in_array($role,["Merchandiser",'Merchandiser Head']))
			return redirect()->route('merchandiser-dashboard');
		else
			return redirect()->route('dashboards');
		

	}
}

