<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\VendorSettlement;
use App\ShopingCart;


class VendorSettlementController extends Controller{

	public function index(){
		$data = $this->vendotSettlement(0)
					 ->whereHas('product',function($q){})
					 ->with(['product'=>function($q){
					 	$q->select('product_id','product_code','product_name');
					 },'vendor'=>function($q){
					 	$q->select('vendor_id','vendor_code','Company_name');
					 }])
					 ->select("order_id","product_id","vendor_id","cart_id")
		     		 ->get(5);
		return view('vendorsettlement.vendorsettlement-list',['data'=>$data]);
	}

	public function vendorsettlement($cartId){
		$data = ShopingCart::where('cart_id',$cartId)
						   ->with(['product'=>function($q){
					 			$q->select('product_id','product_code','product_name');
					 		},'vendor'=>function($q){
					 			$q->select('vendor_id','vendor_code','Company_name');
					 		}])
						    ->select("order_id","product_id","vendor_id","cart_id")
						    ->first();
		$settlementData = config('custom.vendor_settlement_type');
		return view('vendorsettlement.vendorsettlement',['data'=>$data,'settlementData'=>$settlementData]);		
	}

	public function vendorsettlementsave(Request $request){
		$data = $request->all()+['wastagepercen'=>"100"];
		VendorSettlement::create($data);
		ShopingCart::where('cart_id',$data['cart_id'])
		           ->update(['vendor_settlement'=>1]);
		return redirect()->route('vendor-settlement-list')->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
	}
}
