<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use App\EcommerceInteraction;
use App\Orders;
use App\ShopingCart;

use Auth;
class Controller extends BaseController
{
	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	public function interaction($data){
		$obj = new EcommerceInteraction();
		$obj->order_id = $data['order_id'];
		$obj->shoppingcart_id = $data['shoppingcart_id'];
		$obj->user_id = Auth::user()->id;
		$obj->role = Auth::user()->role;
		$obj->ecommerce_status = $data['ecommerce_status'];
		$obj->ecommerce_sub_status = $data['ecommerce_sub_status'];
		$obj->remarks = $data['remarks'];
		$obj->save();
		return $obj;
	}

	public function updateshoppingcart($cart_id,$data){
	//print_r($data); die('kkk'); die('ccvbcfgf');		
		return ShopingCart::where('cart_id',$cart_id)
			  	          ->update($data);
	}

	public function orderdata($id,$substatus=null,$select,$all=0,$orderId=null,$cart_id=null){
		return Orders::whereHas('shoppingcart',function($q) use ($id,$substatus){
         			    $q->where('ecommerce_status',$id);
			 		 	$q->where(function($q) use ($substatus){
			 		 		if(!empty($substatus)){
			 		 			$q->where('ecommerce_sub_status',$substatus);
			 		 		}
			 		 	});
         			  })->with(['shoppingcart.product','shoppingcart'=>function($q) use ($id,$substatus,$all,$cart_id){
         			  		if($all===1){
		         			    $q->where('ecommerce_status',$id);
					 		 	$q->where(function($q) use ($substatus){
					 		 		if(!empty($substatus)){
					 		 			$q->where('ecommerce_sub_status',$substatus);
					 		 		}
					 		 	});
				 		 	}
				 		 	$q->where(function($q) use ($cart_id){
				 		 		if(!empty($cart_id)){
				 		 			$q->where('cart_id',$cart_id);
				 		 		}
			 		 		});
         			  }])
		              ->whereIn('status',['payment_received','offline_payment_requested'])
		              ->where(function($q) use ($orderId){
		              	if(!empty($orderId)){
		              		$q->where('order_id',$orderId);
		              	}
		              })
                      ->orderBy('order_date','desc');

		/*return Orders::whereHas('shoppingcart',function($q) use ($id,$substatus){		
		               $q->where('account_is_received','=','1');         
         			    
         			  })->with(['shoppingcart.product','shoppingcart'=>function($q) use ($id,$substatus){
         			    $q->where('account_is_received','=','1'); 		 	
         			  }])
		              ->whereIn('status',['payment_received','offline_payment_requested'])
                      ->orderBy('order_date','desc');*/
        /*return Orders::orderWithProduct()
					//->whereIn('sha_orders.status',['payment_received',['offline_payment_requested']])
			 		 ->where('ecommerce_status',$id)
			 		 ->where(function($q) use ($substatus){
			 		 	if(!empty($substatus)){
			 		 		$q->where('ecommerce_sub_status',$substatus);
			 		 	}
			 		 })
			 		 ->orderBy('order_date','desc')
			 		 ->select($select+['order_id'=>'sha_orders.order_id',['product_id'=>"sha_shoppingcarts.product_id"]]);*/
	}

  public function orderdatapendding($id,$substatus=null,$select){
		return Orders::whereHas('shoppingcart',function($q) use ($id,$substatus){
         			   
         			    $q->where('account_is_received','=','1');
         			    $q->whereNull('vendor_id');
			 		 	
         			  })->with(['shoppingcart.product','shoppingcart'=>function($q) use ($id,$substatus){       			    
         			    $q->where('account_is_received','=','1');
         			    $q->whereNull('vendor_id');
			 		 	
         			  }])
		              ->whereIn('status',['payment_received','offline_payment_requested'])
                      ->orderBy('order_date','desc');       
	}



   public function orderdata2($id,$substatus=null,$select){
		return Orders::whereHas('shoppingcart',function($q) use ($id,$substatus){
         			   
         			    $q->where('account_is_received','=','1');
			 		 	
         			  })->with(['shoppingcart.product','shoppingcart'=>function($q) use ($id,$substatus){       			    
         			    $q->where('account_is_received','=','1');
			 		 	
         			  }])
		              ->whereIn('status',['payment_received','offline_payment_requested'])
                      ->orderBy('order_date','desc');       
	}

   public function orderdata1($id,$substatus=null,$select){
   	
		return Orders::whereHas('shoppingcart',function($q) use ($id,$substatus){
         			    $q->where('ecommerce_status',$id);
			 		 	$q->where(function($q) use ($substatus){
			 		 		if(!empty($substatus)){
			 		 			$q->where('ecommerce_sub_status',$substatus);
			 		 		}
			 		 	});
         			  })->with(['shoppingcart.product','shoppingcart'=>function($q) use ($id,$substatus){
         			    $q->where('ecommerce_status',$id);
			 		 	$q->where(function($q) use ($substatus){
			 		 		if(!empty($substatus)){
			 		 			$q->where('ecommerce_sub_status',$substatus);
			 		 		}
			 		 	});
         			  }])
		              ->whereIn('status',['payment_receive','offline_payment_requested'])
                      ->orderBy('order_date','desc');
       
	}

	public function statusAndSubstatus($id){
		$status = config('custom.ecommerce_status')[$id];
		$subStatus = config('custom.ecommerce_sub_status')[key($status)];
		return ['status'=>$status,'substatus'=>$subStatus];
	}

	public function shippingdata($order_id){
		return ShopingCart::where('order_id',$order_id)
						  ->get(["cart_id"]);
	}

	public function checkShoppingCartstatus($orderId){
		$data = ShopingCart::where('order_id',$orderId)
				   		   //->where('ecommerce_status','<',$status-1)
						   ->Select(\DB::raw("min(ecommerce_status) as status"))
				   		   ->first();
		return json_decode(json_encode($data),1)['status'];
		
	}

	public function orderUpdateStatus($orderId,$status){
		return Orders::where('order_id',$orderId)
			  		 ->update(['ecommerce_status'=>$status]);
	}

	public function vendotSettlement($settleId){
		return ShopingCart::where(function($q){
                            	$q->where('ecommerce_status','=',3);
                                $q->where('ecommerce_sub_status','=',1);
                                $q->Orwhere(function($q1){
                                    $q1->Orwhere('ecommerce_status','>=',4);
                                });
                           })
                           ->where('vendor_settlement',$settleId);
	}
	

}
