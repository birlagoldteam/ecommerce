<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Orders;
use App\Vendors;
use App\ShopingCart;
use DB;use PDF;

class EcommerceController extends Controller{

	protected $select = ["order_date","invoice","product_sku","cart_id"];

	public function accountOrders(){
		$data = Orders::whereIn('status',['payment_received','offline_payment_requested','Pending'])
		             ->where('pay_received',0)
			 		 ->orderBy('order_date','desc')
			 		 ->select(["order_date","order_id","invoice","order_amount","amount_received"])
			 		 ->get();
		$status = $this->statusAndSubstatus(1);
		$arr = ['data'=>$data]+$status;
		return view('orders.account-list',$arr);

	}
	
	public function updateAccountStatus(Request $request){
		$data = $request->except('_token');
		$shopping = $this->shippingdata($data['order_id']);
		foreach ($shopping as $key => $value) {
			$interaction = $this->interaction($data+['shoppingcart_id'=>$value->cart_id]);
			$arr = array_diff_key($data+['last_interaction_id'=>$interaction->id],['remarks'=>"","order_id"=>""]);
			$this->updateshoppingcart($value->cart_id,$arr);
		}
		$ecommerce_status = $this->checkShoppingCartstatus($data['order_id']);
		Orders::where('order_id',$data['order_id'])
			   ->update(['pay_received'=>$data['ecommerce_sub_status'],'ecommerce_status'=>($ecommerce_status)]);
		return redirect()->back()->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
	}


	public function merchandiserOrders(){
		$data = $this->orderdata(1,1,$this->select,0)->get();
		$vendor = Vendors::where('status','Active')
		                 ->select('vendor_id','vendor_code','Company_name')
		                 ->get();
		$status = $this->statusAndSubstatus(2);
		$arr = ['data'=>$data,'vendor'=>$vendor]+$status;
		return view('orders.merchandiser-list',$arr);
	}

	public function updateMerchandiserStatus(Request $request){
		$ponumber = $request['ponumber'];
		$data = $request->except('_token');
		$interaction = $this->interaction($data);
		$arr = array_diff_key(array_merge($data,['po_number'=>$ponumber,'last_interaction_id'=>$interaction->id,'po_date'=>date('Y-m-d',strtotime($request->processing_date))]),['remarks'=>"",'shoppingcart_id'=>"","ponumber"=>"","podates"=>""]);
		$this->updateshoppingcart($data['shoppingcart_id'],$arr);
		$ecommerce_status = $this->checkShoppingCartstatus($data['order_id']);
		$this->orderUpdateStatus($data['order_id'],$ecommerce_status);
		$this->purchaseOrderGeneratePdf($data['shoppingcart_id']);
		return redirect()->route('merchandiser')->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
	}

	public function purchaseOrderGeneratePdf($cartId) {
		$data = ShopingCart::where('cart_id',$cartId)->with(['vendor'=>function($q){
					$q->select(["Company_name","vendor_id","work_address","work_address1","work_city","work_state","work_pincode"]);
				},'product'=>function($q){
					$q->select(["product_name","product_sku","product_id"]);
				}])->first();
		$pdf = PDF::loadView('orders.pdf_purchase_order', ['data'=>$data]);
		file_put_contents('public/purchase_orders/product_'.$cartId.'.pdf', $pdf->output());
		return $pdf->stream(); 
	}

	public function merchandiserReceivedOrder() {
		$order_id = request()->segment(2);
		$product_id = request()->segment(3);
		$data = $this->orderdata(2,1,$this->select,1,$order_id,$product_id)->get();
		$status = $this->statusAndSubstatus(3);
		return view('orders.merchandiser-received-list',['data'=>$data]+$status);
	}

	public function updateMerchandiserOrderReceived(Request $request){
     	$data = $request->except('_token');
       	$interaction = $this->interaction($data);
		$arr = array_diff_key(array_merge($data,['last_interaction_id'=>$interaction->id]),['remarks'=>"",'shoppingcart_id'=>""]);
		$this->updateshoppingcart($data['shoppingcart_id'],$arr);
		$ecommerce_status = $this->checkShoppingCartstatus($data['order_id']);
		$this->orderUpdateStatus($data['order_id'],$ecommerce_status);
		return redirect()->route('merchandiser-received')->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
	}


	public function merchandiserQcOrders(){
		$order_id = request()->segment(2);
		$cart_id = request()->segment(3);
		$data = $this->orderdata(3,1,$this->select,1,$order_id,$cart_id)->get();
		$vendor = Vendors::where('status','Active')
		                 ->select('vendor_id','vendor_code','Company_name')
		                 ->get();
		$status = $this->statusAndSubstatus(4);
		$arr = ['data'=>$data,'vendor'=>$vendor]+$status;
		return view('orders.merchandiser-qc-list',$arr);
	}

	public function updateMerchandiserQcOrders(Request $request){
		$data = $request->except('_token');
		$interaction = $this->interaction($data);
		$arr = array_diff_key(array_merge($data,['last_interaction_id'=>$interaction->id]),['remarks'=>"",'shoppingcart_id'=>""]);
		$this->updateshoppingcart($data['shoppingcart_id'],$arr);
		$ecommerce_status = $this->checkShoppingCartstatus($data['order_id']);
		$this->orderUpdateStatus($data['order_id'],$ecommerce_status);
		return redirect()->route('merchandiser-qc')->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
	}

	public function finalBillingOrders(){
        $data = Orders::where('ecommerce_status',4)
                      ->select("order_date","invoice","order_id")
                      ->get();
        return view('accounts.final_billing_orders')->with(['data'=>$data]);
    }

    public function generateInvoice($orderId) {
        $data = Orders::where('order_id','=',$orderId)->with(['shoppingcart.product','customer'])->first();
        return view('accounts.generate_invoice')->with(['data'=>$data]);
    }

    public function invoiceGenerated(Request $request) {
        try {
        	$data = Orders::where('order_id','=',$request['order_id'])->with(['shoppingcart.product','customer'])->first();
            $pdf = PDF::loadView('accounts.pdf_generate_invoice', ['data'=>$data]);
            file_put_contents('public/generated_invoices/invoice_'.$data->order_id.'.pdf', $pdf->output());
            $shopping = $this->shippingdata($request['order_id']);
            foreach ($shopping as $key => $value) {
                $data = ['shoppingcart_id'=>$value->cart_id,'order_id'=>$request['order_id'],'ecommerce_status'=>5,'ecommerce_sub_status'=>1,'remarks'=>"Invoice Generated"];
                $interaction = $this->interaction($data);
                $arr = array_diff_key($data+['last_interaction_id'=>$interaction->id],['remarks'=>"","order_id"=>"",'shoppingcart_id'=>""]);
                $this->updateshoppingcart($value->cart_id,$arr);
            }
            $ecommerce_status = $this->checkShoppingCartstatus($data['order_id']);
            Orders::where('order_id',$request['order_id'])->update(['invoice_generated'=>1,'invoice_no'=>$request['invoice_no'],'generated_invoice_date'=>date('Y-m-d'),'ecommerce_status'=>$ecommerce_status]);
            $request->session()->flash('invoiceGenerated','yes');
            return $pdf->stream(); 
            
        } catch(Exception $e) {
            echo $e->message();

        }
        
    }

    public function checkInvoiceGenerated(){
    	$datetime = time();
    	$checkdata = request()->session()->has('invoiceGenerated');
    	/*echo "<pre>";
    	print_r(request()->session()->all());
    	if($checkdata===false){
    		return $this->checkInvoiceGenerated();
    		if(time()>=($datetime+15)){
    			break;
    		}
    	}
    	dd($checkdata);*/
    	return redirect()->route('merchandiser-qc')->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
    }


	public function operationDispatch(){
		$database=env("DB_DATABASE2");
		/*$data = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 5 and ecommerce_sub_status = 1)then 1 else 0 end) as checkvalue,order_id as orderval FROM $database.sha_shoppingcarts group by order_id having cnt=checkvalue) as checkorder"),function($join) use($database){
                	$join->on("$database.sha_orders.order_id", '=', 'checkorder.orderval');
            })
			->select("order_date","invoice","order_id")
			->get();*/
		$data = Orders::where('ecommerce_status',5)
				      ->select("order_date","invoice","order_id")
					  ->get();
		$status = $this->statusAndSubstatus(6);
		$arr = ['data'=>$data]+$status;
		return view('orders.operation-dispatch-list',$arr);
	}

	public function saveOperationDispatch(Request $request){
		$data = $request->except('_token');
		$shopping = $this->shippingdata($data['order_id']);
		foreach ($shopping as $key => $value) {
			$interaction = $this->interaction($data+['shoppingcart_id'=>$value->cart_id]);
			$arr = array_diff_key($data+['last_interaction_id'=>$interaction->id],['remarks'=>"","order_id"=>"","way_bill_no"=>""]);
			$this->updateshoppingcart($value->cart_id,$arr);
		}
		$ecommerce_status = $this->checkShoppingCartstatus($data['order_id']);
		Orders::where('order_id',$data['order_id'])
			   ->update(['way_bill_no'=>$data['way_bill_no'],'ecommerce_status'=>$ecommerce_status]);
		return redirect()->back()->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
	}

	public function operationDispatchStatus(){
		$database=env("DB_DATABASE2");
		/*$data = Orders::join(DB::raw("(SELECT count(order_id) as cnt, sum(case when (ecommerce_status = 6 and ecommerce_sub_status = 1)then 1 else 0 end) as checkvalue,order_id as orderval FROM $database.sha_shoppingcarts group by order_id having cnt=checkvalue) as checkorder"),function($join) use($database){
                	$join->on("$database.sha_orders.order_id", '=', 'checkorder.orderval');
            })
			->select("order_date","invoice","order_id")
			->get();*/
		$data = Orders::where('ecommerce_status',6)
				      ->select("order_date","invoice","order_id")
					  ->get();
		$status = $this->statusAndSubstatus(7);
		$arr = ['data'=>$data]+$status;
		return view('orders.operation-dispatch-success-list',$arr);
	}

	public function saveOperationDispatchStatus(Request $request){
		$data = $request->except('_token');
		$shopping = $this->shippingdata($data['order_id']);
		foreach ($shopping as $key => $value) {
			$interaction = $this->interaction($data+['shoppingcart_id'=>$value->cart_id]);
			$arr = array_diff_key($data+['last_interaction_id'=>$interaction->id],['remarks'=>"","order_id"=>"","way_bill_no"=>""]);
			$this->updateshoppingcart($value->cart_id,$arr);
		}
		$ecommerce_status = $this->checkShoppingCartstatus($data['order_id']);
		Orders::where('order_id',$data['order_id'])
			   ->update(['ecommerce_status'=>$ecommerce_status]);
		return redirect()->back()->with(['message'=>"Data Updated Successfuly!!!","alert"=>"success"]);
	}



	public function productdetails(Request $request){
		$cart_id = $request->cart_id;
		$database=env("DB_DATABASE2");
		$data = ShopingCart::join("$database.sha_products","$database.sha_shoppingcarts.product_id","=","$database.sha_products.product_id")
						  ->join("$database.sha_categories","$database.sha_categories.category_id","=","$database.sha_products.category_id")
						  ->join("$database.sha_productimages","$database.sha_productimages.product_id","=","$database.sha_products.product_id")
						  ->leftJoin(DB::raw("(SELECT distinct(category_id) as size_id FROM $database.sha_size) size"),function($join) use($database){
                				$join->on("$database.sha_products.category_id", '=', 'size.size_id');
            			   })
						   ->where('cart_id',$cart_id)
						   ->select("product_type","$database.sha_categories.category_id","category","$database.sha_categories.category","size_id","$database.sha_shoppingcarts.po_number","$database.sha_productimages.imagename","$database.sha_products.product_id","$database.sha_products.product_sku","$database.sha_shoppingcarts.weight","$database.sha_shoppingcarts.size",DB::raw("DATE_FORMAT($database.sha_shoppingcarts.po_date,'%d-%m-%Y') as podate"))
							->first();
		return $data;

	}
}