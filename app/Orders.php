<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model{

	protected $connection = 'mysql2';
   	protected $primaryKey = 'order_id';
   	public $table="sha_orders";
	public $timestamps = false;

	public static function orderWithProduct(){
		$database=env("DB_DATABASE2");
		return static::join("$database.sha_shoppingcarts","$database.sha_shoppingcarts.order_id","=","$database.sha_orders.order_id")
				     ->join("$database.sha_products","$database.sha_shoppingcarts.product_id","=","$database.sha_products.product_id");
	}

	public function shoppingcart(){
		return $this->hasMany('App\ShopingCart', 'order_id', 'order_id');
	}

	public function customer(){
		return $this->hasOne('App\OrdersUser', 'user_id', 'user_id');
	}
}
