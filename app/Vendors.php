<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    protected $connection = 'mysql2';
   	protected $primaryKey = 'vendor_id';
   	public $table="sha_vendor";
	public $timestamps = false;

	public function shoppingcart(){
		return $this->hasOne('App\ShopingCart', 'vendor_id');
	}
}
