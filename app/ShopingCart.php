<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopingCart extends Model
{
    protected $connection = 'mysql2';
   	protected $primaryKey = 'cart_id';
   	public $table="sha_shoppingcarts";
	public $timestamps = false;

	public function order(){
    	return $this->belongsTo('App\Orders', 'order_id');
    }

    public function product(){
    	return $this->belongsTo('App\Products', 'product_id');
    }

    public function vendor(){
        return $this->belongsTo('App\Vendors','vendor_id','vendor_id');
    }


}
