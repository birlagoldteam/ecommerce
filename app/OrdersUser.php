<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class OrdersUser extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'mysql2';
    protected $primaryKey = 'user_id';
    public $table="sha_users";
}
