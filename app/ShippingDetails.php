<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingDetails extends Model{

	protected $connection = 'mysql2';
   	protected $primaryKey = 'shipping_id';
   	public $table="sha_shippingdetails";
	public $timestamps = false;
}
