<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorSettlement extends Model
{
    protected $connection = 'mysql2';
   	protected $primaryKey = 'id';
   	public $table="sha_vendor_settlement";
	public $timestamps = true;
	protected $fillable = ['order_id','cart_id','settlement_type','invoice_no','product_weight','rate','tax','tax_rate','subtotal','total','settlement','wastagepercent','wastagegold','totalwastagegold','wastagegoldsettle'];
}
