<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $connection = 'mysql2';
   	protected $primaryKey = 'product_id';
   	public $table="sha_products";
	public $timestamps = false;

	public function shoppingcart(){
		return $this->belongsTo('App\ShopingCart','product_id');
	}
}
