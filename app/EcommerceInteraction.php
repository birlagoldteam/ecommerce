<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcommerceInteraction extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    public $table="ecommerce_interaction";
}
